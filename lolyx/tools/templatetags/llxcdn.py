# -*- coding: utf-8 -*-
#
# Copyright (c) 2014-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Template tags for cdn URL generation
#
from random import randrange
from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def cdn_url():
    """
    In case of multiple of CDN_URL are defined in settings, return a random one
    """
    try:
        force = settings.CDN_STATIC_URL_FORCE
    except:
        force = False
    
    if settings.DEBUG and not force:
        return settings.STATIC_URL
    else:
        try:
            url = settings.CDN_STATIC_URL
        except:
            url = settings.STATIC_URL
                    
        if isinstance(url, tuple):
            #  url is a list, take a random element
            rdx = randrange(len(url))
            return url[rdx]
        else:
            # url is a string return it
            return url



