# -*- coding: utf-8 -*-
#
# Copyright (c) 2014-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The job views
"""
import logging
import json
from django.conf import settings
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseForbidden, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import ListView, DetailView, DeleteView, RedirectView
from django.db.models import Count
from django.db.models import Q
from lolyx.llx.models import Company, UserProfile, UserPoint, Region, Tool
from lolyx.job.models import Job, JobTool, JobStat
from lolyx.job.models import JobVote
from lolyx.job.models import JobOnline
from lolyx.job.models import JobBookmark
from lolyx.job.models import JobUserView
from lolyx.job.models import JobRepublishKey
from lolyx.job.forms import JobForm
from lolyx.job.forms import JobAuthForm
from lolyx.utils.view_mixins import CacheMixin
from lolyx.utils.strgen import clean_urlfilter
from lolyx.utils.strgen import clean_urlfilter_str
from django.contrib.auth.decorators import login_required

logger = logging.getLogger(__name__)

class JobListJSONMixin(CacheMixin, object):
    def render_to_response(self, context):
        "Returns a JSON response containing 'context' as payload"
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        "Construct an `HttpResponse` object."
        return HttpResponse(content,
                            content_type='application/json',
                            **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        atools = []
        otools = []
        jobs = []

        for job in context['object_list']:
            jobs.append({"id": job.id,
                         "url": job.get_absolute_url(),
                         "title": job.title,
                         "poste": job.poste,
                         "contact": {"email": job.contact_email,
                                     "tel": job.contact_tel},
                         "description": job.description,
                         "expire": '{0.year}/{0.month}/{0.day}'.format(job.date_expired),
                         "publish": '{0.year}/{0.month}/{0.day}'.format(job.date_published)
                         })

        tags = []
        for tool in context['atools']:
            atools.append({"slug": tool['tool__slug'], "name": tool['tool__name'], "count": tool['tool__id__count']})
        for tool in context['othertools']:
            otools.append({"slug": tool['tool__slug'], "name": tool['tool__name'], "count": tool['tool__id__count']})


        try:
            for tag in self.request.GET['t'].split(','):
                if tag != '':
                    tags.append({'type': 'tool', 'key': tag})
        except:
            pass

        filters = [tags]

        datas = {'filters': filters,
                 'jobs': jobs,
                 'atools': atools,
                 'otools': otools}
        return json.dumps(datas)


class JobPublicListView(JobListJSONMixin, ListView):
    model = Job
    template_name = 'job/public_list.html'

    qsjob = None

    def get_queryset(self):
        qs = super(JobPublicListView, self).get_queryset()
        atools = []
        try:
            atools = self.request.GET['t'].split(',')
            atools.remove(u'')
        except:
            pass

        try:
            search_str = self.request.GET['search']
        except:
            search_str = None

        if len(atools):
            jqs = JobTool.objects.filter(job__family=0,
                                         job__status=settings.JOB_STATUS_PUBLISHED)
            ftool = atools.pop()
            ids = jqs.filter(tool__slug=ftool).values_list('job__id', flat=True)
            for tool in atools:
                ods = jqs.filter(tool__slug=tool).values_list('job__id', flat=True)
                ids = list(set(ids).intersection(ods))

            qs = qs.filter(id__in=list(set(ids)))
        else:
            qs = qs.filter(family=0,
                           status=settings.JOB_STATUS_PUBLISHED)

        if search_str is not None:
            qs = qs.search(search_str)

        qs = qs.select_related('company')
        qs = qs.only('company__name', 'title', 'date_published', 'family', 'description')
        qs = qs.order_by('-date_published')
        self.qsjob = qs
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(JobPublicListView, self).get_context_data(**kwargs)
        # Jobs already viewed by a user
        if self.request.user.is_authenticated():
            context['jobviewed'] = JobUserView.objects.filter(
                user=self.request.user,
                job__status=settings.JOB_STATUS_PUBLISHED).values_list('job__id', flat=True)
        else:
            context['jobviewed'] = []
        #
        context['url'] = self.request.path_info
        filter_tools = []

        try:
            uto = self.request.GET['t']
            filter_tools = clean_urlfilter(uto)
            context['urlfilter'] = clean_urlfilter_str(uto)
        except:
            context['filter_tool'] = ''

        # tools in filter
        context['filter_tools'] = filter_tools

        # active tools in filter
        toolist = JobTool.objects.filter(job__id__in=self.qsjob).values_list('tool__slug', 'tool__name')  # noqa
        context['tools'] = toolist.order_by('tool__name').distinct()

        context['nbjobs'] = len(self.qsjob)
        return context

    def render_to_response(self, context):
        # Look for a 'format=json' GET argument
        if self.request.GET.get('format', 'html') == 'json':
            return JobListJSONMixin.render_to_response(self, context)
        else:
            return ListView.render_to_response(self, context)


class JobArchiveListView(ListView):
    model = Job
    template_name = 'job/private_archive_list.html'

    def get_queryset(self):
        return Job.objects.filter(author=self.request.user,
                                  status=settings.JOB_STATUS_ARCHIVED).order_by('-date_published')


class JSONResponseMixin(object):
    def render_to_response(self, context):
        "Returns a JSON response containing 'context' as payload"
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        "Construct an `HttpResponse` object."
        return HttpResponse(content,
                            content_type='application/json',
                            **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        return serializers.serialize("json", context['object_list'])


class JobUserViewList(JSONResponseMixin, ListView):

    model = Job
    template_name = 'job/jobuserview_list.html'

    def get_queryset(self):
        return JobUserView.objects.filter(user=self.request.user,
                                          job__status=settings.JOB_STATUS_PUBLISHED).order_by('-datetime')

    def render_to_response(self, context):
        # Look for a 'format=json' GET argument
        if self.request.GET.get('format', 'html') == 'json':
            return JSONResponseMixin.render_to_response(self, context)
        else:
            return ListView.render_to_response(self, context)


class JobModeratorListView(ListView):
    model = Job
    template_name = 'job/private_moderation_list.html'

    def get_queryset(self):
        return Job.objects.filter(status=settings.JOB_STATUS_MODERATION).order_by('-date_published')

    def get_context_data(self, *args, **kwargs):
        context = super(JobModeratorListView, self).get_context_data(**kwargs)
        moderator = self.request.user.groups.filter(name=settings.GROUP_MODERATOR)
        userprof = UserProfile.objects.get(user=self.request.user)
        allowed = userprof.jobmoder_allowed
        if moderator or allowed:
            context['votes'] = JobVote.objects.filter(job__status=settings.JOB_STATUS_MODERATION,
                                                      user=self.request.user).values_list('job__id', flat=True)
            return context
        else:
            raise PermissionDenied


class JobPublicView(DetailView):
    """The job view in a public context
    """
    model = Job

    def get_context_data(self, **kwargs):
        context = super(JobPublicView, self).get_context_data(**kwargs)
        # Test if job is published
        if self.object.is_publish:
            context['othertools'] = JobTool.objects.filter(
                job__family=self.object.family,
                job__status=settings.JOB_STATUS_PUBLISHED
                ).values('tool__name'
                         ).annotate(Count('tool__id')).order_by('-tool__id__count')
            context['company_jobs'] = Job.objects.filter(
                status=settings.JOB_STATUS_PUBLISHED,
                company=self.object.company).only('title', 'family').exclude(id=self.object.id)
            context['jobtools'] = JobTool.objects.filter(job=self.object).select_related('tool').order_by('-level')

            if self.request.user.is_authenticated():
                JobUserView.objects.create(user=self.request.user, job=self.object)
                context['bookmarked'] = JobBookmark.objects.filter(user=self.request.user, job=self.object).exists()

            # log that someone view the job
            JobStat.objects.create(job=self.object)

            return context
        else:
            # Return a 404 to purge public search engine
            raise Http404("No such page!")


class JobBookmarkDeleteView(DeleteView):
    model = JobBookmark
    success_url = reverse_lazy('profile')

    def get_queryset(self):
        """ Limit a User to only modifying their own data. """
        qs = super(JobBookmarkDeleteView, self).get_queryset()
        return qs.filter(user=self.request.user)


class JobModeratorView(DetailView):
    """The job view in moderation by moderator

    TODO add protection, only moderator can access
    """
    model = Job

    def get_context_data(self, **kwargs):
        context = super(JobModeratorView, self).get_context_data(**kwargs)
        moderator = self.request.user.groups.filter(name=settings.GROUP_MODERATOR)
        userprof = UserProfile.objects.get(user=self.request.user)
        context['votes'] = JobVote.objects.filter(job=self.object, user=self.request.user).count()
        allowed = userprof.jobmoder_allowed
        if moderator or allowed:
            if self.object.in_moderation:
                return context
            else:
                raise PermissionDenied
        else:
            raise PermissionDenied


class JobPrivateView(DetailView):
    """View by an authenticated user who owns the job
    """
    model = Job

    def get_context_data(self, *args, **kwargs):
        context = super(JobPrivateView, self).get_context_data(**kwargs)
        context['jobtools'] = JobTool.objects.filter(job=self.object)
        # Security to edit only own resume
        self.object = self.get_object()
        if self.request.user.id != self.object.author.id:
            raise Http404("No such page !")
        else:
            return context


class JobEditView(JobPrivateView):
    """View by an authenticated user who owns the job
    """
    template_name = 'job/job_edit.html'


def new(request):
    """
    Create a new job
    """
    if request.user.is_authenticated():
        return new_auth(request)
    else:
        raise PermissionDenied


def new_auth(request):
    """
    Create a new job for authenticated users
    """
    if request.method == 'POST':
        form = JobAuthForm(request.POST)
        if form.is_valid():
            job = Job.objects.create(author=request.user,
                                     title=form.cleaned_data['title'],
                                     company=form.cleaned_data['company'],
                                     family=form.cleaned_data['family'],
                                     description=form.cleaned_data['poste_desc'],
                                     description_markup=form.cleaned_data['desc_markup'],
                                     ref=form.cleaned_data['ref'],
                                     creation=form.cleaned_data['creation'],
                                     salaire=form.cleaned_data['salaire'],
                                     teletravail=form.cleaned_data['teletravail'],
                                     lat=form.cleaned_data['lat'],
                                     lon=form.cleaned_data['lon'])
            return redirect(job.get_edit_url())
    else:
        form = JobAuthForm()
        comps = Company.objects.filter(user=request.user).order_by('name')
        form.fields['company'].queryset = comps
        if 'c' in request.GET:
            form.fields['company'].initial = request.GET['c']
        if len(comps) == 1:
            form.fields['company'].initial = comps[0].pk
            form.fields['contact_name'].initial = comps[0].contact_name
            form.fields['contact_tel'].initial = comps[0].contact_tel
            form.fields['contact_email'].initial = comps[0].contact_email
            form.fields['lon'].initial = comps[0].lon
            form.fields['lat'].initial = comps[0].lat

    return render(request,
                  'job/job_new_auth.html',
                  {'form': form})


def publish(request, pk):
    """Publish a job

    @params Object request
    @params integer pk

    Check if the user is a moderator or has the minimal karma to do
    auto moderation
    """
    if request.user.is_authenticated():
        job = get_object_or_404(Job, pk=pk)
        # set the default url
        url = job.get_absolute_url()
        # Is the user a moderator
        if request.user.groups.filter(name=settings.GROUP_MODERATOR):
            # The job must be in moderation status
            if job.status == settings.JOB_STATUS_MODERATION:
                job.publish_moderator()
                JobVote.objects.create(job=job,
                                       user=request.user,
                                       vote=settings.MDR_MODERATOR_POINT)
        else:
            # Is the user the owner
            if request.user == job.author:
                # Does the user has minimal karma
                userprof = UserProfile.objects.get(user=request.user)
                if userprof.karma > settings.KARMA_AUTO_MODERATE:
                    job.publish_moderator()
                else:
                    job.moderate()
                    url = '/accounts/profile/'
            else:
                userprof = UserProfile.objects.get(user=request.user)
                if userprof.jobmoder_allowed:
                    # The job must be in moderation status
                    if job.status == settings.JOB_STATUS_MODERATION:
                        job.publish_user(request.user)
                        url = "{}moderate/".format(job.get_absolute_url())

        return redirect(url)
    else:
        logger.debug('not auth')
        return redirect('/accounts/profile/')


def unpublish(request, pk):
    """Unpublish a job

    The job will not be displayed on public site anymore
    """
    job = get_object_or_404(Job, pk=pk)
    url = job.get_absolute_url()
    if request.user.is_authenticated():
        # The job must be online
        if job.status == settings.JOB_STATUS_PUBLISHED:
            # Is the user a moderator
            if request.user.groups.filter(name=settings.GROUP_MODERATOR):
                job.unpublish()
                url = '/accounts/profile/'
                return redirect(url)
            else:
                # Is the user the owner
                if request.user == job.author:
                    # Does the user has minimal karma
                    job.unpublish()
                    url = '/accounts/profile/'
                    return redirect(url)
                else:
                    return HttpResponseForbidden()
        elif job.status == settings.JOB_STATUS_MODERATION:
            if request.user == job.author:
                # Does the user has minimal karma
                job.unpublish()
                url = '/accounts/profile/'
                return redirect(url)
        else:
            return HttpResponseForbidden()
    else:
        logger.debug('not auth')
        url = '/accounts/profile/'
        return redirect(url)


@login_required
def refuse(request, pk):
    """Refuse a job

    @params Object request
    @params integer pk

    """
    job = get_object_or_404(Job, pk=pk)
    url = "{}moderate/".format(job.get_absolute_url())
    # Is the user a moderator
    if request.user.groups.filter(name=settings.GROUP_MODERATOR):
        # The job must be in moderation status
        if job.status == settings.JOB_STATUS_MODERATION:
            job.refuse_moderator(request.user)
            return redirect(url)
        else:
            return HttpResponseForbidden()
    else:
        userprof = UserProfile.objects.get(user=request.user)
        if userprof.jobmoder_allowed:
            # The job must be in moderation status
            if job.status == settings.JOB_STATUS_MODERATION:
                job.refuse_user(request.user)
                return redirect(url)
            else:
                return HttpResponseForbidden()
        else:
            return HttpResponseForbidden()


def copy(request, pk):
    """
    Copy a job

    TODO secure this
    """
    job = get_object_or_404(Job, pk=pk)
    job.pk = None  # awesome copy method
    job.status = settings.JOB_STATUS_EDITION
    job.author = request.user
    job.viewed = 0
    job.moderation_score = 0
    job.moderation_votes = 0
    job.cost = 0
    if job.lon * job.lat == 0:
        job.lon = job.company.lon
        job.lat = job.company.lat
    job.save()
    for jbt in JobTool.objects.filter(job_id=pk):
        jbt.pk = None
        jbt.job = job
        jbt.save()

    return redirect('/accounts/profile/')


def bookmark(request, pk):
    """Bookmark a job
    """
    job = get_object_or_404(Job, pk=pk)
    JobBookmark.objects.get_or_create(job=job, user=request.user)
    return redirect(job.get_absolute_url())


@cache_page(settings.JOB_TEMPLATE_DEFAULT_CACHE)
def stats(request, family, indicator):
    """Return stats

    Nombres d'offres en ligne par <indicator>
    """
    datas = []
    fam = settings.FAMILY_IDS[family]
    if indicator == 'region':
        regions = Region.objects.all()
        jobs = Job.objects.filter(family=fam,
                                  status=settings.JOB_STATUS_PUBLISHED).values('region').annotate(Count('region'))

        for region in regions:
            nb = 0
            for job in jobs:
                if job['region'] == region.id:
                    nb = job['region__count']

            datas.append({'name': region.name,
                          'insee': region.insee,
                          'offres': nb})
    elif indicator == 'coord':
        jobs = Job.objects.filter(family=fam,
                                  status=settings.JOB_STATUS_PUBLISHED)
        jobs = jobs.select_related('company__name')
        jobs = jobs.values_list('lon', 'lat', 'title', 'company__name', 'id')
        jobs = jobs.exclude(Q(lon__isnull=True) | Q(lat__isnull=True))
        for job in jobs:
            datas.append({'lon': job[0],
                          'lat': job[1],
                          'title': job[2],
                          'company': job[3],
                          'url': reverse('job', args=[job[4]])
                          })

    return HttpResponse(json.dumps(datas),
                        mimetype='application/json')


@cache_page(settings.JOB_TEMPLATE_DEFAULT_CACHE)
def jobtool_public(request, family):
    """Return stats

    All tools indicates as requitrements in published jobs
    """
    datas = []
    tools = JobTool.objects.filter(job__family=family,
                                   job__status=settings.JOB_STATUS_PUBLISHED)

    for tool in tools:
        datas.append({'name': tool.tool.name,
                      'id': tool.tool.id})

    return HttpResponse(json.dumps(datas),
                        mimetype='application/json')


@login_required
def myjobs(request):
    """Return myjobs
    """
    moder = []
    datas = []
    jobs = Job.objects.filter(company__user=request.user)

    for job in jobs:
        votes = []
        accept = 0
        refuse = 0
        if job.status == settings.JOB_STATUS_MODERATION:
            for vote in JobVote.objects.filter(job=job).values('vote').annotate(Count('vote')):
                if (vote['vote'] == 1):
                    accept = vote['vote__count']
                else:
                    refuse = vote['vote__count']
            moder.append({'id': job.id,
                          'score': job.moderation_score,
                          'accept': accept,
                          'refuse': refuse,
                          'votes': accept + refuse
                          })

    datas.append({"moderation": moder})

    return HttpResponse(json.dumps(datas),
                        content_type='application/json')


def republish(request, pk, key):
    """Republish a job with a single URL without authentification

    The job will be republished
    """
    jobkey = get_object_or_404(JobRepublishKey, job_id=pk, key=key)
    job = get_object_or_404(Job, pk=pk)
    url = job.get_absolute_url()

    # The job must be online
    if job.status == settings.JOB_STATUS_ARCHIVED:
        job.republish()
        jobkey.delete()
        return redirect(url)
    else:
        return HttpResponseForbidden()


def job_stat_json(request, pk):
    """Return stats for a single job

    """
    job = Job.objects.filter(id=pk).values_list('viewed')
    if len(job):
        datas = {'status': 0, 'views': job[0][0]}
    else:
        datas = {'status': 1, 'views': 0}

    return HttpResponse(json.dumps(datas),
                        mimetype='application/json')
