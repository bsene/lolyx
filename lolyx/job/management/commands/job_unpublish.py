#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Publish a job
"""
from optparse import make_option
from django.conf import settings
from django.core.management.base import BaseCommand
from lolyx.job.models import Job
from lolyx.job.models import JobRepublishKey


class Command(BaseCommand):
    help = 'Unpublished job that expired'

    option_list = BaseCommand.option_list + (
        make_option("-j",
                    "--jobid",
                    dest="jobid",
                    type="int",
                    help="Job unique identifier",
                    default=None),
        )


    def handle(self, *args, **options):
        """
        Handle the command
        """
        if options['jobid'] is not None:
            job = Job.objects.get(pk=options['jobid'])
            job.unpublish()
