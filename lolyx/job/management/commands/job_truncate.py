#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Remove from online jobs taht have expired

A job is published for a fixed number of days defined in settings,
after this period if the job is still online it will be unpublished
"""
import sys
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Unpublished job that expired'

    def handle(self, *args, **options):
        """
        Handle the command
        """
        tables = ['job_jobonline',
                  'job_jobtool',
                  'job_joblang',
                  'job_jobuserview',
                  'job_job']
        
        for table in tables:
            sys.stdout.write('TRUNCATE %s CASCADE;\n' % table)
