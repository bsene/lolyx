#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Create sample jobs


"""
import logging
from optparse import make_option
from random import randrange
from django.db import connection
from django.conf import settings
from django.core.management.base import BaseCommand
from lolyx.llx.models import Tool
from django.contrib.auth.models import User
from lolyx.llx.models import UserProfile
from lolyx.llx.models import UserPoint
from lolyx.llx.models import Company
from lolyx.job.models import Job
from lolyx.job.models import JobTool
from faker import Faker


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Unpublished resume that have expired'

    option_list = BaseCommand.option_list + (
        make_option("-n",
                    "--nbvalues",
                    dest="nbvalues",
                    type="int",
                    help="number of values to input",
                    default=1),
        make_option("-c",
                    "--company",
                    dest="company",
                    type="int",
                    help="number of values to input",
                    default=0),
        )

    def force_emailverified(self):
        """Force all emails are to be considered as verified
        """
        sql = "UPDATE account_emailaddress SET verified = true"
        cursor = connection.cursor()
        cursor.execute(sql)

    def next_id(self, sql):
        """Read the last used id from the sequence, return next usable id
        """
        cursor = connection.cursor()
        cursor.execute(sql)
        ids = cursor.fetchone()
        nextval = ids[0] + 1
        return nextval

    def create_users(self, nb):
        """Create the users in database
        """
        sql = "SELECT last_value FROM auth_user_id_seq"
        firstid = self.next_id(sql)

        i = firstid
        last = nb + firstid

        users = []
        profiles = []
        points = []
        batch = 100
        ibatch = 0

        while i < last:
            user = User(username="login%s" % (i), email="email%s" % (i))
            users.append(user)
            ibatch = ibatch + 1

            userp = UserProfile(user_id=i,
                                status=settings.USER_PROFILE_COMPANY)
            profiles.append(userp)

            points.append(UserPoint(user_id=i, points=4200))

            if ibatch > batch:
                User.objects.bulk_create(users)
                UserProfile.objects.bulk_create(profiles)
                UserPoint.objects.bulk_create(points)
                users = []
                profiles = []
                points = []
                ibatch = 0

            i = i + 1

        if len(users):
            User.objects.bulk_create(users)
            UserProfile.objects.bulk_create(profiles)
            UserPoint.objects.bulk_create(points)

        return firstid

    def create_companies(self, nb, first_userid):
        """Create a batch of nb companies in database
        """
        f = Faker()
        userid = randrange(nb) + first_userid

        sql = "SELECT last_value FROM llx_company_id_seq"
        firstid = self.next_id(sql)

        i = firstid
        last = nb + firstid
        companies = []
        batch = 100
        ibatch = 0
        while i < last:
            name = f.company()
            companies.append(Company(user_id=userid,
                                     name=name,
                                     website=f.url(),
                                     ulule=name.lower().startswith('a'),
                                     description=" ".join(f.words(120)),
                                     lon=0.14707,
                                     lat=49.50835))
            i = i + 1
            if ibatch > batch:
                Company.objects.bulk_create(companies)
                batch = []
                ibatch = 0

        if len(companies):
            Company.objects.bulk_create(companies)

        return firstid

    def rlon(self):
        """Random longitude
        """
        return -1.0 + float(randrange(600)) / 100.0

    def rlat(self):
        """Random latitude
        """
        return 42.3 + float(randrange(600)) / 100.0

    def handle(self, *args, **options):
        """
        Handle the command
        """
        if not settings.DEBUG:
            print "Debug tools, quit now\n"
            exit(0)

        i = 0
        f = Faker()

        first_userid = self.create_users(options['nbvalues'])
        self.force_emailverified()
        first_compid = self.create_companies(options['nbvalues'], first_userid)

        while i < options['nbvalues']:
            i = i + 1

            userid = randrange(options['nbvalues']) + first_userid
            if options["company"] > 0:
                companyid = options["company"]
            else:
                companyid = randrange(options['nbvalues']) + first_compid

            job = Job.objects.create(author_id=userid,
                                     company_id=companyid,
                                     family=randrange(3),
                                     contact_name=f.last_name(),
                                     contact_email=f.email(),
                                     title=" ".join(f.words(3)),
                                     description=" ".join(f.words(2000)),
                                     lon=self.rlon(),
                                     lat=self.rlat(),
                                     salaire=f.random_int()
                                     )

            # set some standard tool
            pktools = [159, 157, 1, 182, 214, 320, 104, 100]
            
            for tool in Tool.objects.filter(pk__in=pktools):
                try:
                    JobTool.objects.create(job=job,
                                           tool=tool,
                                           level=1+randrange(4))
                except:
                    pass

            for tool in Tool.objects.all().order_by('?')[:1+randrange(19)]:
                try:
                    JobTool.objects.create(job=job,
                                           tool=tool,
                                           level=1+randrange(4))
                except:
                    pass

            job.publish_moderator()

            print job.title, job.lon, job.lat
