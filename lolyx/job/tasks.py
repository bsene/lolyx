# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Tasks related to jobs
"""
import logging
from celery.task import task
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from django.template.loader import render_to_string
import urllib
from lolyx.routing.utils import readjson
from lolyx.utils.twitter import update_status as twitter_update_status
from lolyx.llx.models import CompanyRessource
import json


logger = logging.getLogger(__name__)


@task()
def mail_moderation(job):
    """Send an email when a job enter in moderation status

    A mail is sent to all users with moderator status
    """
    group = Group.objects.get(name=settings.GROUP_MODERATOR)
    moderators = User.objects.filter(groups__in=[group.id])

    for moder in moderators:
        logger.debug("send email to [%s]" % (moder.email))

        tmpl_subj = "job/emails/job_moderation_subject.txt"
        tmpl_body = "job/emails/job_moderation_body.txt"

        subject = render_to_string(tmpl_subj, {'job': job})
        msg = render_to_string(tmpl_body, {'job': job,
                                           'email': moder.email})

        send_mail(subject.strip(),
                  msg,
                  settings.EMAIL_FROM, [moder.email])


@task()
def tweet_job(job, tool_list):
    """Post a tweet

    job : (model) Job

    """
    from lolyx.utils.twitter import update_status
    company = job.company


    # Fetch twitter account)
    cras = CompanyRessource.objects.filter(company=job.company,
                                          ressource_id=20)

    if len(cras) > 0:
        account = cras[0]

        # fetch llxio
        job_url = job.get_absolute_url()

        qurl = urllib.quote("http://lolix.org{}".format(job_url))
        llxiourl = "http://llx.io/?url={}".format(qurl)
        url = json.loads(readjson(llxiourl))
        msg = "@%s cherche %s https://llx.io/%s" % (account, job.title, url['short_url'])

        tool = tool_list.pop()
        while (len(msg) + len(tool) + 1 ) < 140:
            msg = "%s #%s" % (msg, tool)
            tool = tool_list.pop()

        twitter_update_status(msg)
        print "%s %s " % (len(msg), msg)


@task()
def mail_job_is_online(job):
    """Send an email to job author

    A mail is sent to te author to inform her/him thit his/her job is online
    """

    logger.debug("send email to [%s]" % (job.contact_email))

    tmpl_subj = "job/emails/job_online_subject.txt"
    tmpl_body = "job/emails/job_online_body.txt"

    subject = render_to_string(tmpl_subj, {'job': job})
    msg = render_to_string(tmpl_body, {'job': job})

    send_mail(subject.strip(),
              msg,
              settings.EMAIL_FROM, [job.contact_email])
