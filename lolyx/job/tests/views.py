# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test views
"""
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.test import TestCase
from django.test import Client
from django.test import RequestFactory
from django.http import Http404
from lolyx.job.models import Job, JobStat, JobUserView, JobTool
from lolyx.llx.models import UserExtra
from lolyx.llx.models import UserProfile
from lolyx.llx.models import Company, Tool
from lolyx.job import views


class ViewsTests(TestCase):
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        JobStat.objects.all().delete()
        Job.objects.all().delete()
        JobTool.objects.all().delete()
        UserExtra.objects.all().delete()
        Group.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

        self.moder = User.objects.create_user('moderator',
                                              'admin_search@modo.com',
                                              'admintest')

        group = Group.objects.create(name=settings.GROUP_MODERATOR)
        self.moder.groups.add(group)

        self.company = Company.objects.create(user=self.user,
                                              name='Foo corp',
                                              contact_name='Foo')

    def test_job_store_stat(self):
        """Job public detail view

        When accessing this view we log a line in table JobStat
        if the user is not authenticated

        Here is not auth so not log
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_PUBLISHED)

        request = RequestFactory().get('/fake-path')
        request.user = self.moder
        view = views.JobPublicView.as_view()
        # Run.
        response = view(request, pk=job.id)
        nbs = JobStat.objects.all().count()
        self.assertEqual(nbs, 0)
        self.assertTrue(self.moder.is_authenticated)

    def test_job_view_unpublished(self):
        """The job is not published

        Raise a 404 if try to view an unpublished job
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_MODERATION)

        request = RequestFactory().get('/fake-path')
        view = views.JobPublicView.as_view()
        # Run.
        with self.assertRaises(Http404):
            response = view(request, pk=job.id)

    def test_publish_moderator(self):
        """Publish a job

        - the user is a moderator

        Result : the job is published
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_MODERATION)

        request = RequestFactory().get('/fake-path')
        # Publish with a moderator
        request.user = self.moder
        response = views.publish(request, job.id)

        njob = Job.objects.get(pk=job.id)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(njob.status, settings.JOB_STATUS_PUBLISHED)

    def test_publish_moderator_wronstatus(self):
        """Publish a job

        - a moderator can't publish a job if it's not in moderation

        Result : the job is published
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_EDITION)

        request = RequestFactory().get('/fake-path')
        # Publish with a moderator
        request.user = self.moder
        response = views.publish(request, job.id)

        njob = Job.objects.get(pk=job.id)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(njob.status, settings.JOB_STATUS_EDITION)

    def test_publish_regular(self):
        """Publish a job

        - the user is a regular with no karma

        Result : flag moderation is set on job's status
        """
        regular_user = User.objects.create_user('publish_regular',
                                                'admin_search@bar.com',
                                                'admintest')

        job = Job.objects.create(author=regular_user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_EDITION)

        request = RequestFactory().get('/fake-path')
        request.user = regular_user

        response = views.publish(request, job.id)

        njob = Job.objects.get(pk=job.id)

        self.assertTrue(request.user.is_authenticated())
        self.assertEqual(response.status_code, 302)
        self.assertEqual(njob.status, settings.JOB_STATUS_MODERATION)

    def test_unpublish_regular(self):
        """Unpublish a job

        - the user is a regular user

        Result : flag moderation is set on job's status
        """
        regular_user = User.objects.create_user('publish_regular',
                                                'admin_search@bar.com',
                                                'admintest')

        job = Job.objects.create(author=regular_user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_PUBLISHED)

        request = RequestFactory().get('/fake-path')
        request.user = regular_user

        response = views.unpublish(request, job.id)

        njob = Job.objects.get(pk=job.id)

        self.assertTrue(request.user.is_authenticated())
        self.assertEqual(response.status_code, 302)
        self.assertEqual(njob.status, settings.JOB_STATUS_ARCHIVED)

    def test_new_auth(self):
        """Call the form to create a new job
        """
        user = User.objects.create_user('new_auth',
                                        'admin_search@bar.com',
                                        'admintest')

        request = RequestFactory().get('/fake-path')
        request.user = self.user

        response = views.new(request)
        self.assertEqual(response.status_code, 200)

    def test_new_auth_withcompany(self):
        """Call the form to create a new job

        - the user has created a company before
        """
        user = User.objects.create_user('withcompany',
                                        'admin_search@bar.com',
                                        'admintest')

        company = Company.objects.create(user=user,
                                         name='Company name',
                                         contact_name='Foo contact name')

        request = RequestFactory().get('/fake-path')
        request.user = user

        response = views.new(request)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, company.contact_name)

    def test_job_privateview(self):
        """Job private detail view
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        request = RequestFactory().get('/fake-path')
        request.user = self.user

        view = views.JobPrivateView.as_view()
        # Run.
        response = view(request, pk=job.id)

        self.assertEqual(response.status_code, 200)

    def test_job_privateview_notowner(self):
        """Job private detail view

        - only the owner can view his job in a private view, raise a
          404 error otherwise
        """
        user = User.objects.create_user('notowner',
                                        'admin_search@bar.com',
                                        'admintest')

        job = Job.objects.create(author=user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        request = RequestFactory().get('/fake-path')
        request.user = self.user

        view = views.JobPrivateView.as_view()

        with self.assertRaises(Http404):
            response = view(request, pk=job.id)

    def test_copy(self):
        """Copy a job
        """
        regular_user = User.objects.create_user('publish_regular',
                                                'admin_search@bar.com',
                                                'admintest')

        job = Job.objects.create(author=regular_user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_EDITION)

        before = Job.objects.all().count()

        request = RequestFactory().get('/fake-path')
        request.user = regular_user

        response = views.copy(request, job.id)

        after = Job.objects.all().order_by('-id')

        self.assertTrue(request.user.is_authenticated())
        self.assertEqual(len(after), before + 1)
        self.assertEqual(after[0].lat, 0.0)
        self.assertEqual(after[0].lon, 0.0)

    def test_copy_geo(self):
        """Copy a job

        The job is geolocated
        The job as tool associated to it
        """
        regular_user = User.objects.create_user('publish_regular',
                                                'admin_search@bar.com',
                                                'admintest')

        tool1 = Tool.objects.create(name='foo1', slug='foo1')
        tool2 = Tool.objects.create(name='foo2', slug='foo2')

        job = Job.objects.create(author=regular_user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_EDITION,
                                 lon=2.65,
                                 lat=42.06)

        JobTool.objects.create(job=job, tool=tool1)
        JobTool.objects.create(job=job, tool=tool2)

        before = Job.objects.all().count()

        request = RequestFactory().get('/fake-path')
        request.user = regular_user

        response = views.copy(request, job.id)

        after = Job.objects.all()
        jobtools = JobTool.objects.all().count()

        self.assertTrue(request.user.is_authenticated())
        self.assertEqual(len(after), before + 1)
        self.assertEqual(after[0].lat, 42.06)
        self.assertEqual(after[0].lon, 2.65)
        # the job tools we copied too so we have now 4 tools in DB
        self.assertEqual(jobtools, 4)
