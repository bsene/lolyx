/*
 *
 *
 *
 *
 */

function deltool(id) {

    if (id > 0) {
	var url = "deltool/"+id;
	$.get(url,
	      function (data) {
		  /* if the tool is correctly added
		   * result is > 0
		   */
		  if (data.result == 1) {
		      $('#job-tool-'+id).remove();
		  }
	      }, 'json');
    }
};

function leveltool(id, level) {

    if (id > 0) {
	var url = "lvltool/"+id+"/"+level;
	$.get(url,
	      function (data) {
		  /* if the tool is correctly added
		   * result is > 0
		   */
		  if (data.result > 0 ) {
		      $('#job-tool-btn-'+id).append('<span class="glyphicon glyphicon-star"></span>');
		  }
	      }, 'json');
    }
};
