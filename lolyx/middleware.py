from django.db import connection
from django.template import Template, Context

class SQLLogMiddleware:

    def process_response ( self, request, response ):
        time = 0.0
        for q in connection.queries:
            time += float(q['time'])

        t = Template('''<div id="sqlfooter"><p><em>Total sql query count:</em> {{ count }}, 
        <em>Total sql execution time:</em> {{ time }} ms</p></div>
        ''')
            
        response.content = '%s%s' % (
            response.content.decode('utf-8'),
            t.render(Context({
                'count':len(connection.queries),
                'time':time})))
        return response
