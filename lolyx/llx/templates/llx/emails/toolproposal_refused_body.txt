Nous sommes au regret de vous annoncer que votre proposition n'a pas
été acceptée lolix.org

{{name}} : {{url}}

Si vous pensez vraiment que cette proposition doit être retenue et
qu'elle n'est pas déjà présente dans la base, la discussion est
ouverte.

Merci d'avoir enrichit Lolix !

--
{{user.first_name}} {{user.last_name}}
email : {{user.email}}
