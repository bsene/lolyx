# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Lolyx/llx
"""
# Models
from lolyx.llx.tests.company import CompanyTests
from lolyx.llx.tests.contract import ContractTests
from lolyx.llx.tests.diplom import DiplomTests
from lolyx.llx.tests.effectif import EffectifTests
from lolyx.llx.tests.faq import FaqTests
from lolyx.llx.tests.invitation import InvitationTests
from lolyx.llx.tests.motd import MotdTests
from lolyx.llx.tests.sector import SectorTests
from lolyx.llx.tests.tool import ToolTests
from lolyx.llx.tests.toolproposal import ToolProposalTests
from lolyx.llx.tests.typent import TypentTests
from lolyx.llx.tests.userextra import UserExtraTests
from lolyx.llx.tests.userpoint import UserPointTests
from lolyx.llx.tests.userprofile import UserProfileTests
# Non models
from lolyx.llx.tests.command import CommandTests
from lolyx.llx.tests.urls import UrlsTests
from lolyx.llx.tests.views import ViewsTests
from lolyx.llx.tests.forms import FormsTests
