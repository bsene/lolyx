# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test all public and private urls
"""
import json
from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from lolyx.llx.models import Company
from lolyx.llx.models import UserProfile
from lolyx.llx.models import Effectif
from lolyx.llx.models import Typent
from lolyx.llx.models import Poste, Sector, Tool
from lolyx.job.models import Job


class UrlsTests(TestCase):
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        Company.objects.all().delete()
        Poste.objects.all().delete()
        Sector.objects.all().delete()
        Tool.objects.all().delete()
        self.url_server = "http://testserver"
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'barfoo')

    def test_home(self):
        """
        The home
        """
        client = Client()
        response = client.get('/')

        self.assertContains(response, 'Lolix', status_code=200)

    def test_list(self):
        """
        All companies
        """
        Company.objects.create(user=self.user, name='iNued5oo')

        client = Client()
        response = client.get('/c/')

        self.assertContains(response, 'iNued5oo', status_code=200)

    def test_detail_pk(self):
        """
        A company by it's id
        """
        company = Company.objects.create(user=self.user, name='iNued5oo')

        Job.objects.create(author=self.user,
                           company=company, title='foo', status=1)

        client = Client()
        response = client.get('/c/{}/'.format(company.id))

        self.assertContains(response, 'iNued5oo', status_code=200)

    def test_detail_slug(self):
        """
        A company by it's slug
        """
        company = Company.objects.create(user=self.user,
                                         name='iNued5oo',
                                         slug='Chieb9Em')
        Job.objects.create(author=self.user,
                           company=company, title='foo', status=1)

        client = Client()
        response = client.get('/c/{}'.format(company.slug))

        self.assertContains(response, 'iNued5oo', status_code=200)

    def test_detail_view_empty(self):
        """
        A company with no offer online
        """
        company = Company.objects.create(user=self.user,
                                         name='iNued5oo',
                                         slug='Chieb9Em',
                                         stay_alive=False)

        client = Client()
        response = client.get(company.get_absolute_url())

        self.assertEqual(response.status_code, 302)

    def test_company_beegees(self):
        """
        This is a BeeGee's company
        Ah ah ah Staying Alive !!!
        """
        company = Company.objects.create(user=self.user,
                                         name='iNued5oo',
                                         slug='Chieb9Em',
                                         stay_alive=True)

        client = Client()
        response = client.get(company.get_absolute_url())
        self.assertContains(response, 'iNued5oo', status_code=200)

    def test_company_edit_notlogged(self):
        """Try to edit a company without been logged
        """
        company = Company.objects.create(user=self.user,
                                         name='iNued5oo')

        url = '/c/{}/edit/'.format(company.id)

        client = Client()
        response = client.get(url)

        location = "{}/accounts/login/?next={}".format(self.url_server, url)

        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.has_header('Location'))
        self.assertEqual(response.__getitem__('Location'), location)

    def test_company_edit_right_user(self):
        """Edit a company with the right user
        """
        company = Company.objects.create(user=self.user,
                                         name='iNued5oo')

        url = '/c/{}/edit/'.format(company.id)

        client = Client()
        client.login(username='foobar', password='barfoo')

        response = client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_company_edit_unauth_user(self):
        """Edit a company with an unauthorized users
        """
        User.objects.create_user('edit_unauth_user',
                                 'admin_search@bar.com',
                                 'barfoo')

        company = Company.objects.create(user=self.user,
                                         name='iNued5oo')

        url = '/c/{}/edit/'.format(company.id)

        client = Client()
        auth = client.login(username='edit_unauth_user', password='barfoo')

        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 404)

    def test_company_edit_post_valid(self):
        """Do a POST on Company edit FORM

        All mandatory values are set
        """
        Effectif.objects.create(name='effe1')
        Sector.objects.create(name='sector1')
        Typent.objects.create(name='typent1')

        otheruser = User.objects.create_user('editpost',
                                             'admin_search@bar.com',
                                             'barfoo')

        company = Company.objects.create(user=otheruser,
                                         name='iNued5oo')

        url = '/c/{}/edit/'.format(company.id)

        params = {'name': 'new corp',
                  'effectif': '1',
                  'typent': '1',
                  'sector': '1',
                  'desc_mark': '1',
                  'lon': '0.0',
                  'lat': '0.0'}

        client = Client()
        auth = client.login(username='editpost', password='barfoo')

        response = client.post(url, params)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)

    def test_company_edit_post_invalid(self):
        """Do a POST on Company edit FORM

        Some datas are missing
        """
        otheruser = User.objects.create_user('editpost',
                                             'admin_search@bar.com',
                                             'barfoo')

        company = Company.objects.create(user=otheruser,
                                         name='iNued5oo')

        url = '/c/{}/edit/'.format(company.id)

        params = {'name': 'new corp',
                  'desc_mark': '1'}

        client = Client()
        auth = client.login(username='editpost', password='barfoo')

        response = client.post(url, params)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)

    def test_company_new(self):
        """Get the form for Company creation
        """
        User.objects.create_user('edit_unauth_user',
                                 'admin_search@bar.com',
                                 'barfoo')
        url = '/c/new/'

        client = Client()
        auth = client.login(username='edit_unauth_user', password='barfoo')

        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)

    def test_company_new_post(self):
        """Do a POST on Company creation FORM

        All mandatory fields are filled
        """
        Effectif.objects.create(name='effe1')
        Sector.objects.create(name='sector1')
        Typent.objects.create(name='typent1')
        User.objects.create_user('newposte',
                                 'admin_search@bar.com',
                                 'barfoo')
        url = '/c/new/'

        params = {'name': 'new corp',
                  'effectif': '1',
                  'typent': '1',
                  'sector': '1',
                  'desc_mark': '1',
                  'lon': '0.0',
                  'lat': '0.0'}

        client = Client()
        auth = client.login(username='newposte', password='barfoo')

        before = Company.objects.all().count()

        response = client.post(url, params)

        after = Company.objects.all().count()

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(before, 0)
        self.assertEqual(after, 1)

    def test_company_new_post_invalid(self):
        """Do a POST on Company creation FORM

        Some fields are missing
        """
        User.objects.create_user('newposte',
                                 'admin_search@bar.com',
                                 'barfoo')
        url = '/c/new/'

        params = {'name': 'new corp'}

        client = Client()
        auth = client.login(username='newposte', password='barfoo')

        before = Company.objects.all().count()

        response = client.post(url, params)

        after = Company.objects.all().count()

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(before, 0)
        self.assertEqual(after, 0)

    def test_profile_set(self):
        """Set a user profile
        """
        otheruser = User.objects.create_user('edit_unauth_user',
                                             'admin_search@bar.com',
                                             'barfoo')

        Company.objects.create(user=self.user, name='iNued5oo')

        url = '/accounts/profile/?set=2'

        client = Client()
        auth = client.login(username='edit_unauth_user', password='barfoo')

        response = client.get(url)

        nuser = UserProfile.objects.get(user=otheruser)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(nuser.status, 2)

    def test_stats_main_emploi(self):
        """The main stats
        """
        url = '/stats/main_emploi.json'

        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

    def test_stats_main_stage(self):
        """The main stats
        """
        url = '/stats/main_stage.json'

        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

    def test_stats_main_mission(self):
        """The main stats
        """
        url = '/stats/main_mission.json'

        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

    def test_stats_toolmap(self):
        """The main stats
        """
        url = '/stats/toolmap_emploi.json'

        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

    def test_ressource_tool(self):
        """The ressources

        Will return a json contains all Tool that name begin with an 'a'
        """
        Tool.objects.create(name='git', slug='git')
        Tool.objects.create(name='apache', slug='apa')
        Tool.objects.create(name='Ada', slug='ada')

        url = '/ressources/t/a'

        client = Client()
        response = client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(2, len(data['datas']))

    def test_ressource_poste(self):
        """The Poste ressources

        Will return a json contains all Poste that name begin with an 'd'
        """
        Poste.objects.create(name='devOps', slug='devops')
        Poste.objects.create(name='Admin/Sys', slug='adminsys')
        Poste.objects.create(name='Administrateur', slug='admin')

        url = '/ressources/p/d'

        client = Client()
        response = client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(1, len(data['datas']))

    def test_ressource_sector(self):
        """The Sector ressources

        Will return a json contains all Sector that name begin with an 'd'
        """
        Sector.objects.create(name='Indus1', slug='sector1')
        Sector.objects.create(name='Indus2', slug='sector2')
        Sector.objects.create(name='Indus3', slug='sector3')
        Sector.objects.create(name='Commerce', slug='sector4')

        url = '/ressources/s/i'

        client = Client()
        response = client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(3, len(data['datas']))
