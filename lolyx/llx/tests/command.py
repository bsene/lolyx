# -*- coding: utf-8 -*-
#
# Copyright (c) 2013, 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Lolyx llx management commands
"""
from datetime import datetime
from StringIO import StringIO
from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth.models import User
from lolyx.llx.models import Tool, ToolAssociated
from lolyx.llx.models import UserProfile, UserPoint, Company
from lolyx.resume.models import Resume
from lolyx.job.models import Job, JobTool


class CommandTests(TestCase):
    """Test the management commands in usermgt
    """
    def setUp(self):
        """
        Init
        """
        Tool.objects.all().delete()
        Job.objects.all().delete()
        JobTool.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

        self.company = Company.objects.create(user=self.user, name='Foo corp')

    def test_tool_list(self):
        """tool_list management command
        """
        Tool.objects.create(name='tool1', slug='tool1')
        Tool.objects.create(name='tool2', slug='tool2')

        attend = '1 tool1\n2 tool2\n'

        content = StringIO()
        call_command('tool_list', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)

    def test_user_points_not_updated_date(self):
        """
        Set nb points for each users
        The nb of point is not updated becaus of date
        """
        userprof = UserProfile.objects.get(user=self.user)
        userpts = UserPoint.objects.get(user=self.user)

        userpts.active = True
        userpts.points = userprof.monthly_points - 100
        userpts.save()

        before = userpts.points

        content = StringIO()
        call_command('user_points', stdout=content)
        content.seek(0)

        userpts = UserPoint.objects.get(user=self.user)

        self.assertEqual(userpts.points, before)

    def test_user_points_updated(self):
        """
        Set nb points for each users
        The nb of point UPDATED because of date

        - date is before 30 Days
        - points less than quota

        """
        userprof = UserProfile.objects.get(user=self.user)
        userpts = UserPoint.objects.get(user=self.user)

        userpts.active = True
        userpts.points = userprof.monthly_points - 100
        userpts.date_updated = datetime(1970, 1, 1, 12, 30, 45)
        userpts.save()

        content = StringIO()
        call_command('user_points', stdout=content)
        content.seek(0)

        userpts = UserPoint.objects.get(user=self.user)

        self.assertEqual(userpts.points, userprof.monthly_points)

    def test_user_points_not_updated_points(self):
        """
        Set nb points for each users
        The nb of point UPDATED because of date

        - date is before 30 Days
        - points greater than quota

        """
        userprof = UserProfile.objects.get(user=self.user)
        userpts = UserPoint.objects.get(user=self.user)

        userpts.active = True
        userpts.points = userprof.monthly_points + 100
        userpts.date_updated = datetime(1970, 1, 1, 12, 30, 45)
        userpts.save()

        before = userpts.points

        content = StringIO()
        call_command('user_points', stdout=content)
        content.seek(0)

        userpts = UserPoint.objects.get(user=self.user)

        self.assertEqual(userpts.points, before)

    def test_toolassociated(self):
        """
        Associated Tool
        """
        job1 = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        job2 = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        job3 = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        tool1 = Tool.objects.create(name='foo1', slug='fool1')
        tool2 = Tool.objects.create(name='foo2', slug='fool2')
        tool3 = Tool.objects.create(name='foo3', slug='fool3')

        JobTool.objects.create(job=job1, tool=tool1)
        JobTool.objects.create(job=job1, tool=tool2)

        JobTool.objects.create(job=job2, tool=tool2)
        JobTool.objects.create(job=job2, tool=tool3)

        JobTool.objects.create(job=job3, tool=tool2)
        JobTool.objects.create(job=job3, tool=tool3)

        content = StringIO()
        call_command('tool_odds', stdout=content)
        content.seek(0)

        toa = ToolAssociated.objects.get(one=tool1, two=tool2)

        self.assertEqual(toa.odds, 1)

        toa = ToolAssociated.objects.get(one=tool2, two=tool3)

        self.assertEqual(toa.odds, 2)

    def test_tool_odds(self):
        """Tool odds
        """
        job1 = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        job2 = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        tool1 = Tool.objects.create(name='foo1', slug='fool1')
        tool2 = Tool.objects.create(name='foo2', slug='fool2')

        JobTool.objects.create(job=job1, tool=tool1)
        JobTool.objects.create(job=job1, tool=tool2)

        JobTool.objects.create(job=job2, tool=tool2)

        content = StringIO()
        call_command('tool_odds', stdout=content)
        content.seek(0)

        toa = Tool.objects.get(pk=tool1.id)

        self.assertEqual(toa.odds, 1)

        toa = Tool.objects.get(pk=tool2.id)

        self.assertEqual(toa.odds, 2)
