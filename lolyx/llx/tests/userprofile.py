# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Invitation object

"""
from django.contrib.auth.models import User
from django.test import TestCase
from lolyx.llx.models import UserProfile


class UserProfileTests(TestCase):  # pylint: disable-msg=R0904
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        User.objects.all().delete()
        UserProfile.objects.all().delete()

    def test_userprofile_defaults(self):
        """
        Defaults values on User's profile
        """
        user = User.objects.create_user('foobar',
                                        'admin_search@foo.com',
                                        'adminfoo')

        uprof = UserProfile.objects.get(user=user)

        self.assertEqual(uprof.karma, 0)
        self.assertEqual(str(uprof), "foobar's profile")
        self.assertEqual(unicode(uprof), "foobar's profile")
        self.assertFalse(uprof.jobmoder_allowed)
        self.assertFalse(uprof.jobmoder_blacklist)

    def test_give_jobmoder_regular(self):
        """Give the jobmoder permission on regular user

        The permission will be setted
        """
        user = User.objects.create_user('foobar',
                                        'admin_search@foo.com',
                                        'adminfoo')

        uprof = UserProfile.objects.get(user=user)

        uprof.give_jobmoder()  # action

        uprof = UserProfile.objects.get(user=user)

        self.assertTrue(uprof.jobmoder_allowed)
        self.assertFalse(uprof.jobmoder_blacklist)

    def test_give_jobmoder_regular_already(self):
        """Give the jobmoder permission on regular user

        The permission will be setted
        """
        user = User.objects.create_user('foobar',
                                        'admin_search@foo.com',
                                        'adminfoo')

        uprof = UserProfile.objects.get(user=user)
        uprof.jobmoder_allowed = True
        uprof.save()

        uprof.give_jobmoder()  # action

        uprof = UserProfile.objects.get(user=user)

        self.assertTrue(uprof.jobmoder_allowed)
        self.assertFalse(uprof.jobmoder_blacklist)

    def test_give_jobmoder_blacklisteduser(self):
        """Give the jobmoder permission on blacklisted user

        - the user is blacklisted
        The permission will not be setted
        """
        user = User.objects.create_user('foobar',
                                        'admin_search@foo.com',
                                        'adminfoo')

        uprof = UserProfile.objects.get(user=user)
        uprof.jobmoder_blacklist = True
        uprof.save()

        uprof.give_jobmoder()  # action

        uprof = UserProfile.objects.get(user=user)

        self.assertFalse(uprof.jobmoder_allowed)
        self.assertTrue(uprof.jobmoder_blacklist)
