# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for ToolProposal
"""
from django.contrib.auth.models import User
from django.test import TestCase
from lolyx.llx.models import ToolProposal


class ToolProposalTests(TestCase):
    """The main tests
    """
    def setUp(self):
        """Set up the tests
        """
        ToolProposal.objects.all().delete()

        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_create(self):
        """Create a ToolProposal
        """
        tool = ToolProposal.objects.create(user=self.user,
                                           name='git',
                                           slug='git',
                                           url='http://foo.bar',
                                           comment='alpha')

        self.assertTrue(tool.id > 0)
        self.assertEqual(str(tool), 'git')
        self.assertEqual(unicode(tool), 'git')
