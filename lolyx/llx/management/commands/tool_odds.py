#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Update Tool's statistics
"""
from django.core.management.base import BaseCommand
from django.db.models import Count
from lolyx.llx.models import Tool, ToolAssociated
from lolyx.job.models import JobTool


class Command(BaseCommand):
    help = 'Print tools on stdout'

    def handle(self, *args, **options):
        """
        Update the number of use on tools
        """
        ToolAssociated.objects.all().delete()
        jobtools = JobTool.objects.values('tool__id').annotate(Count('tool__id'))

        for jobtool in jobtools:
            tool = Tool.objects.get(pk=jobtool['tool__id'])
            tool.odds = jobtool['tool__id__count']
            tool.save()
            self.associated(tool)
            self.stdout.write('{} {} \n'.format(jobtool['tool__id'],
                                                jobtool['tool__id__count']))

    def associated(self, tool):
        """Update tool's association statistics

        Params :

        tool : dict {'tool__id__count': 5, 'tool__id': 150}
        """
        jobs = JobTool.objects.filter(tool=tool)
        for job in jobs:
            tools = JobTool.objects.filter(job=job).exclude(tool=tool)
            for tox in tools:
                toa, created = ToolAssociated.objects.get_or_create(one=tool,
                                                                    two=tox.tool)
                if created:
                    toa.odds = 1
                else:
                    toa.odds = toa.odds + 1
                toa.save()
