# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models definition for llx app
"""
import markdown
import logging
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from lolyx.llx.tasks import new_company_mail
from lolyx.external.models import Ressource


class Effectif(models.Model):
    """Effectif an enterprise classification
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


class Poste(models.Model):
    """Poste
    """
    name = models.CharField(max_length=30)
    slug = models.SlugField(max_length=30, unique=True)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


class RemoteWork(models.Model):
    """Remote work
    """
    label = models.CharField(max_length=30,
                             verbose_name='Intitulé')


    def __unicode__(self):
        """
        The unicode method
        """
        return self.label

    def __str__(self):
        """
        The unicode method
        """
        return self.label


class Lang(models.Model):
    """Lang
    """
    name = models.CharField(max_length=30)
    code = models.CharField(max_length=2, blank=True, null=True)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


class Sector(models.Model):
    """A sector, an enterprise classification
    """
    name = models.CharField(max_length=30)
    slug = models.SlugField(max_length=30, unique=True)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


class Typent(models.Model):
    """A Typent, an enterprise classification
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


class Company(models.Model):
    """
    The company object
    """
    user = models.ForeignKey(User)
    name = models.CharField(max_length=300,
                            verbose_name='Name')

    description = models.CharField(max_length=3000,
                                   verbose_name='Description',
                                   blank=True,
                                   null=True)

    description_markup = models.IntegerField(default=0,
                                             choices=((0, 'text/plain'),
                                                      (1, 'markdown'),
                                                      (2, 'reST')))

    last_login = models.DateTimeField(auto_now_add=False, blank=True, null=True)
    last_modification = models.DateTimeField(auto_now_add=False, blank=True, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)

    # Can't be set by the user himself
    slug = models.SlugField(max_length=30,
                            verbose_name='slug',
                            unique=True,
                            blank=True,
                            null=True)

    # Internal note
    note = models.CharField(max_length=3000,
                            verbose_name='Note intern',
                            blank=True,
                            null=True)

    # Website url
    website = models.URLField(blank=True, null=True)

    # Flag indiiuant Adherent a l'april
    adh_april = models.BooleanField(default=False,
                                    verbose_name='Adherent april')
    # Est-ce une SSII
    ssii = models.BooleanField(default=False)
    # Est-ce un cabinet de recrutement
    cabrecrut = models.BooleanField(default=False)

    # A contribue a la campagne ulule v2
    ulule = models.BooleanField(default=False)

    # Ah ah ah staying alive
    # affiche la page societe meme si aucune offre n'est en ligne
    stay_alive = models.BooleanField(default=False)

    # Permet à un user de ne plus voir une société dans son profil
    is_deleted = models.BooleanField(default=False)

    adresse = models.CharField(max_length=300, blank=True, null=True)
    cp = models.CharField(max_length=10, blank=True, null=True)
    ville = models.CharField(max_length=30, blank=True, null=True)
    telephone = models.CharField(max_length=30, blank=True, null=True)

    url = models.URLField(max_length=300, blank=True, null=True)

    contact_name = models.CharField(max_length=50,
                                    verbose_name='Contact',
                                    blank=True,
                                    null=True)

    contact_email = models.EmailField(max_length=100,
                                      verbose_name='Contact email',
                                      blank=True,
                                      null=True)

    contact_tel = models.CharField(max_length=30,
                                   verbose_name='Contact tel',
                                   blank=True,
                                   null=True)

    remotework = models.ForeignKey(RemoteWork, default=0)
    sector = models.ForeignKey(Sector, default=0)
    typent = models.ForeignKey(Typent, default=0)
    effectif = models.ForeignKey(Effectif, default=0)

    logo = models.ImageField(null=True, blank=True, upload_to='logo/')

    # Nb of job accepted
    nb_acc = models.PositiveSmallIntegerField(default=0)

    # Nb of job refused
    nb_ref = models.PositiveSmallIntegerField(default=0)

    lon = models.FloatField(default=0.0)
    lat = models.FloatField(default=0.0)

    def __unicode__(self):
        """
        The unicode method
        """
        return self.name

    def __str__(self):
        """
        The unicode method
        """
        return self.name

    def job_refused(self):
        """A job for this company was refused"""
        self.nb_ref = self.nb_ref + 1
        self.save()

    def get_absolute_url(self):
        if self.slug:
            return '/c/{}'.format(self.slug)
        else:
            return '/c/{}/'.format(self.id)

    @property
    def description_markdown(self):
        """Return description formated in Markdown
        """
        return markdown.markdown(self.description)


class CompanyRessource(models.Model):
    """A company account on a social network
    """
    ressource = models.ForeignKey(Ressource)
    company = models.ForeignKey(Company)
    account = models.CharField(max_length=255)

    def __unicode__(self):
        """The unicode method
        """
        return self.account

    def __str__(self):
        """The string method
        """
        return self.account


class Tool(models.Model):
    """The tools known by candidat or required by companies
    """
    name = models.CharField(max_length=30,
                            verbose_name='Tool name',
                            unique=True)

    slug = models.SlugField(max_length=30, unique=True)

    url = models.URLField(max_length=300, blank=True)

    category = models.PositiveSmallIntegerField(default=0)
    family = models.PositiveIntegerField(default=0)

    # is the tool used a lot or not
    odds = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        """
        The unicode method
        """
        return self.name

    def __str__(self):
        """
        The unicode method
        """
        return self.name


class ToolAssociated(models.Model):
    """Odds for tools association
    """
    one = models.ForeignKey(Tool, related_name='one')
    two = models.ForeignKey(Tool, related_name='two')
    odds = models.PositiveIntegerField(default=0)

    # usefull for statistics
    tms = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('one', 'two',)


class ToolProposal(models.Model):
    """A new tool proposed by a user
    """
    user = models.ForeignKey(User)
    name = models.CharField(max_length=30,
                            verbose_name='Tool name')

    slug = models.SlugField(max_length=30, unique=True)

    url = models.URLField(max_length=300,
                          blank=True)

    category = models.PositiveSmallIntegerField(default=0)
    family = models.PositiveIntegerField(default=0)
    # a little commentaire
    comment = models.TextField()

    def __unicode__(self):
        """
        The unicode method
        """
        return self.name

    def __str__(self):
        """
        The unicode method
        """
        return self.name


class Region(models.Model):
    """The regions known by candidat or required by companies
    """
    name = models.CharField(max_length=30,
                            verbose_name='Region name')

    # number of job currently online
    job_online = models.SmallIntegerField(blank=True, null=True)

    # number of resume currently online
    res_online = models.SmallIntegerField(blank=True, null=True)

    insee = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        """
        The unicode method
        """
        return self.name

    def __str__(self):
        """
        The unicode method
        """
        return self.name


class Contract(models.Model):
    """The contract
    """
    code = models.CharField(max_length=30,
                            verbose_name='Contract code')

    def __unicode__(self):
        """The unicode method
        """
        return self.code

    def __str__(self):
        """The string method
        """
        return self.code


class Diplom(models.Model):
    """A diploma
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


class Invitation(models.Model):
    """An invitation send from a company to a candidate
    """
    user_from = models.ForeignKey(User, related_name='from')
    user_to = models.ForeignKey(User, related_name='to')

    status = models.CharField(max_length=30)


class UserExtra(models.Model):
    """Store extra information per user
    """
    user = models.ForeignKey(User)
    extra = models.TextField()


class Faq(models.Model):
    """FAQ

    """
    question = models.CharField(max_length=500)
    answer = models.CharField(max_length=3000)
    score = models.IntegerField(default=0)

    def __unicode__(self):
        """
        The unicode method
        """
        return self.question

    def __str__(self):
        """
        The unicode method
        """
        return self.question


class Motd(models.Model):
    """Messages of the day

    """
    active = models.BooleanField(default=False)
    home = models.BooleanField(default=False)
    title = models.CharField(max_length=300)
    body = models.TextField(max_length=3000)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """
        The unicode method
        """
        return self.title

    def __unicode__(self):
        """
        The unicode method
        """
        return self.title


class UserPoint(models.Model):
    """Points affected to an user
    """
    user = models.ForeignKey(User)
    points = models.PositiveIntegerField(default=settings.USER_INITIAL_POINT)
    active = models.BooleanField(default=False)
    date_updated = models.DateTimeField(auto_now_add=True)

    def remove_points(self, points):
        """A job was published
        """
        self.points = self.points - points
        self.save()

    def add_points(self, points):
        """A job was published
        """
        self.points = self.points + points
        self.save()


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    # User Karma
    karma = models.SmallIntegerField(default=0)
    status = models.PositiveSmallIntegerField(default=0,
                                              choices=((0, 'unknow'),
                                                       (1, 'candidate'),
                                                       (2, 'recruiters')))
    # Nb of points reaffected each month
    monthly_points = models.IntegerField(default=settings.POINTS_MONTHLY_ADD)

    # The user can vote on job moderation
    jobmoder_allowed = models.BooleanField(default=False)

    # The user won't be able to vote anymore
    jobmoder_blacklist = models.BooleanField(default=False)

    # The user won't be able to vote anymore
    jobmoder_point = models.PositiveSmallIntegerField(
        default=settings.MDR_USER_POINT)

    # longitude/latitude
    lon = models.FloatField(default=0.0)
    lat = models.FloatField(default=0.0)

    def __str__(self):
        return "%s's profile" % self.user

    def __unicode__(self):
        return "%s's profile" % self.user

    def give_jobmoder(self):
        """Give the user the permission to do moderation on jobs
        """
        # Check if the user is not blacklisted
        if not self.jobmoder_blacklist:
            # Only do save if needed
            if not self.jobmoder_allowed:
                self.jobmoder_allowed = True
                self.save()


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.get_or_create(user=instance)
        UserPoint.objects.get_or_create(user=instance)

def create_new_company(sender, instance, created, **kwargs):
    if created:
        new_company_mail.delay(instance)

post_save.connect(create_new_company, sender=Company)
post_save.connect(create_user_profile, sender=User)
