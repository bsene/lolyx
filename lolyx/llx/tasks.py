# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Tasks related to llx
"""
import logging
from celery.task import task
from django.conf import settings
from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string


logger = logging.getLogger(__name__)


@task
def new_company_mail(company):
    """Send an email to the user who propose

    company : (model) Company

    """
    logger.debug("New company [%s]" % (company.name))

    company_name = company.name
    sub_tmp = "llx/emails/new_company_subject.txt"
    msg_tmp = "llx/emails/new_company_body.txt"

    subject = render_to_string(sub_tmp, {'name': company_name})

    body = render_to_string(msg_tmp, {'name': company_name})

    mail_admins = (subject.strip(),
                   body)


@task
def toolproposal_accepted(toolp, user):
    """Send an email to the user who propose
    """
    sub_tmp = "llx/emails/toolproposal_accepted_subject.txt"
    msg_tmp = "llx/emails/toolproposal_accepted_body.txt"

    toolproposal_message(toolp, user, sub_tmp, msg_tmp)


@task
def toolproposal_refused(toolp, user):
    """Send an email to the user who propose
    """
    sub_tmp = "llx/emails/toolproposal_refused_subject.txt"
    msg_tmp = "llx/emails/toolproposal_refused_body.txt"

    toolproposal_message(toolp, user, sub_tmp, msg_tmp)


@task
def toolproposal_message(toolp, user, sub_tmp, msg_tmp):
    """Send an email to the user who propose

    toolp : ToolProposal
    user : the user who did the action accept/refuse
    sub_tmp : subject templates
    msg_tmp : body templates
    """
    logger.debug("send email to [%s]" % (toolp.user.email))

    subject = render_to_string(sub_tmp, {'name': toolp.name})

    body = render_to_string(msg_tmp, {'name': toolp.name,
                                      'email': toolp.user.email,
                                      'url': toolp.url,
                                      'user': user})

    email = EmailMessage(subject.strip(),
                         body,
                         settings.EMAIL_FROM,
                         [toolp.user.email],
                         headers={'Reply-To': user.email})
    email.send()
