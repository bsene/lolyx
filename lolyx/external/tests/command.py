# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Lolyx llx management commands
"""
from StringIO import StringIO
from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth.models import User
from lolyx.external.models import Ressource
from lolyx.external.models import UserRessource


class CommandTests(TestCase):
    """
    Test the management commands in usermgt
    """
    def test_external_list(self):
        """external_list management command
        """
        user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

        res1 = Ressource.objects.create(name='external1',
                                        userpage='http://bar.foo/u/{}',
                                        api_url='http://foo.com/{}',
                                        api_active=True)

        useres = UserRessource.objects.create(user=user,
                                              ressource=res1,
                                              userid='foo')

        attend = 'http://bar.foo/u/foo 200 1970-01-01\n'

        content = StringIO()
        call_command('external_list', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)
