# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test all public and private urls
"""
from django.test import TestCase
from django.test import Client
from lolyx.external.models import Ressource


class UrlsTests(TestCase):
    """
    The urls
    """
    def test_public_list(self):
        """Public list

        Show all ressources
        """
        ressource = Ressource.objects.create(name='ResFoobar42')

        client = Client()
        response = client.get('/ressources/')

        self.assertContains(response, 'ResFoobar42', status_code=200)
