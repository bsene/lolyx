# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Plugin for Bitbucket.org
"""
import json
from django.test import TestCase
from lolyx.external.tests.httpserver import TestServer
from lolyx.external.utils.plugins.LyxBitbucket import LyxBitbucket


class LyxBitbucketTests(TestCase):
    """
    The urls
    """

    def test_fetch(self):
        """
        Test the fetch function
        """
        http = TestServer()
        http.start()
        url = 'http://127.0.0.1:%d/fetch' % (http.port)

        plugin = LyxBitbucket()
        (status, datas) = plugin.fetch(url)

        self.assertEqual(status, 200)
        self.assertEqual(datas, {'public_repos': 0, 'type': 'repo'})

    def test_fetch_error404(self):
        """
        Test the fetch function on an invalid URL that returns a 404
        """
        http = TestServer()
        http.start()
        url = 'http://127.0.0.1:%d/error-404' % (http.port)

        plugin = LyxBitbucket()
        (status, datas) = plugin.fetch(url)

        self.assertEqual(status, 404)
        self.assertEqual(datas, {})

    def test_parse(self):
        """
        Successful parse
        """
        data = {"repositories": [{"scm": "hg",
                                  "slug": "mustachebox",
                                  "is_private": False,
                                  "name": "mustachebox",
                                  "language": "python"},
                                 {"scm": "hg",
                                  "slug": "django-mailman",
                                  "is_private": False,
                                  "name": "django-mailman",
                                  "language": "python"}
                                 ]
                }

        plugin = LyxBitbucket()
        response = plugin.parse(json.dumps(data))
        attend = {'type': 'repo', "public_repos": 2}
        self.assertEqual(response, attend)

    def test_parse_failed(self):
        """
        Unsuccessful parse
        """
        plugin = LyxBitbucket()
        response = plugin.parse("None")
        attend = {'type': 'repo', "public_repos": 0}
        self.assertEqual(response, attend)
