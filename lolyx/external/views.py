# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The views of external ressources
"""
import logging
from django.views.generic import ListView
from django.views.generic import DetailView, ListView, CreateView, TemplateView, UpdateView
from django.core.urlresolvers import reverse
from lolyx.utils.view_mixins import ProtectedMixin
from lolyx.external.models import Ressource
from lolyx.external.models import UserRessource
from lolyx.external.forms import UserRessourceForm

logger = logging.getLogger(__name__)


class RessourceListView(ListView):

    model = Ressource

    def get_queryset(self):
        qs = Ressource.objects.filter(active=True).only('name',
                                                        'nb_resume',
                                                        'url',
                                                        'api_active')
        return qs.order_by('name')

class UserRessourceNew(ProtectedMixin, CreateView):
    form_class = UserRessourceForm
    model = UserRessource
    template_name = 'external/userressource_new.html'
    

    def get_form(self, form_class):
        form = form_class(**self.get_form_kwargs())
        user_res = UserRessource.objects.filter(user=self.request.user).values_list('ressource')
        qsres = Ressource.objects.filter(active=True).exclude(pk__in=user_res)
        form.fields['ressource'].queryset = qsres.order_by('name')
        return form
        
    def form_valid(self, form):
        form.instance.user = self.request.user

        self.success_url = reverse('resume_external')
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.

        return super(UserRessourceNew, self).form_valid(form)
