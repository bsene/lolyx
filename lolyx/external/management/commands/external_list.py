#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output urls of external ressources
"""
from django.core.management.base import BaseCommand
from lolyx.external.models import UserRessource


class Command(BaseCommand):
    help = 'Print on stdout all userressource in database'

    def handle(self, *args, **options):
        """
        Handle command
        """
        for res in UserRessource.objects.all():
            line = "%s %s %s\n" % (res.userpage(),
                                   res.last_status, res.last_checked)
            self.stdout.write(line)
