# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.conf.urls import patterns, url
from lolyx.external.views import RessourceListView
from lolyx.external.views import UserRessourceNew

urlpatterns = patterns('',
                       url(r'^$', RessourceListView.as_view(), name='ressources'),
                       url(r'^user/new/$', UserRessourceNew.as_view(), name='user_ressource_new'),
                       )
