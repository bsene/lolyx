# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Plugin for Bitbucket.org
"""
import json
from lolyx.external.utils.LyxExternalPlugin import LyxExternalPlugin


class LyxHumanCoders(LyxExternalPlugin):

    @classmethod
    def fetch(self, url):
        """Fetch the datas

        @parameter : string url

        Return : json object
        """
        datas = {}
        (status, content) = self.download(url)
        if status == 200:
            datas = self.parse(content)
        return (status, datas)

    @classmethod
    def parse(self, data):
        """Parse the datas
        Return : json object
        """
        try:
            val = json.loads(data)
            repo = len(val['repositories'])
        except:
            repo = 0

        return {"type": "repo",
                "public_repos": repo}
