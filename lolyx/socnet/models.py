# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models definition for socnet app, social network app
"""
from django.conf import settings
from django.db import models
from lolyx.llx.models import Company


class Network(models.Model):
    """A social network
    """
    name = models.CharField(max_length=30)
    enable = models.BooleanField(default=True)
    category = models.PositiveSmallIntegerField(default=0)    
    url = models.CharField(max_length=300)
    homepage = models.URLField(max_length=300,
                               blank=True, null=True)

    def __unicode__(self):
        """The unicode method
        """
        return self.name

    def __str__(self):
        """The string method
        """
        return self.name


# class CompanyNetwork(models.Model):
#     """A company account on a social network
#     """
#     network = models.ForeignKey(Network)
#     company = models.ForeignKey(Company)
#     account = models.CharField(max_length=30)

#     def __unicode__(self):
#         """The unicode method
#         """
#         return self.account

#     def __str__(self):
#         """The string method
#         """
#         return self.account
