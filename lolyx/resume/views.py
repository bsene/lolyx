# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The resume views
"""
import logging
import json
from django.conf import settings
from django.shortcuts import (get_object_or_404, redirect)
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse
from django.views.generic import DetailView, ListView, CreateView, UpdateView
from lolyx.resume.models import Resume, ResumeTool
from lolyx.resume.models import ResumeStat
from lolyx.resume.models import ResumeLang
from lolyx.resume.models import ResumeExperience
from lolyx.resume.models import ResumeOnline
from lolyx.resume.forms import ResumeForm, ExperienceForm, PreferenceForm
from lolyx.llx.models import UserExtra, UserProfile, Tool
from lolyx.external.models import UserRessource
from lolyx.utils.view_mixins import ProtectedMixin
from lolyx.utils.strgen import clean_urlfilter

logger = logging.getLogger(__name__)


class ResumeView(DetailView):
    """The public view
    Anonymous mode
    """
    model = Resume
    template_name = 'resume/resume_public.html'

    def get_context_data(self, **kwargs):
        if self.object.status != 1:
            raise Http404("No such page!")
        else:
            context = super(ResumeView, self).get_context_data(**kwargs)
            tools = ResumeTool.objects.filter(resume=self.object).select_related('tool')  # noqa
            context['tools'] = tools.order_by('-level', 'tool__category')
            langs = ResumeLang.objects.filter(resume=self.object).select_related('lang')  # noqa
            langs = langs.values_list('lang__name', 'level', 'lang__code')
            context['langs'] = langs.order_by('-level')
            expes = ResumeExperience.objects.filter(resume=self.object)
            context['expes'] = expes.order_by('-date_deb')

            try:
                ext = UserExtra.objects.get(user=self.object.user)
                extra = json.loads(ext.extra)
            except:
                extra = {}

            for ext in extra:
                extra[ext]['name'] = ext
                key = extra[ext]['type']
                if key in context:
                    context[key].append(extra[ext])
                else:
                    context[key] = []
                context[key].append(extra[ext])

            ResumeStat.objects.create(resume=self.object)
            return context


class ResumeEdit(ProtectedMixin, UpdateView):
    """Edition a resume
    """
    model = Resume
    form_class = ResumeForm
    template_name = 'resume/resume_edit.html'
    success_url = '/accounts/profile/'

    def get(self, *args, **kwargs):
        # Security to edit only own resume
        self.object = self.get_object()
        if self.request.user.id != self.object.user.id:
            return redirect('/accounts/profile')
        return super(ResumeEdit, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ResumeEdit, self).get_context_data(**kwargs)
        context['restools'] = ResumeTool.objects.filter(resume=self.object)
        context['experiences'] = ResumeExperience.objects.filter(resume=self.object)
        try:
            ext = UserExtra.objects.get(user=self.object.user)
            extra = json.loads(ext.extra)
        except:
            extra = {}

        for ext in extra:
            extra[ext]['name'] = ext
            key = extra[ext]['type']
            if key in context:
                context[key].append(extra[ext])
            else:
                context[key] = []
            context[key].append(extra[ext])
        return context


class ResumeEditTools(ProtectedMixin, DetailView):
    model = Resume
    template_name = 'resume/resume_edit_tools.html'

    def get(self, *args, **kwargs):
        # Security to edit only own resume
        self.object = self.get_object()
        if self.request.user.id != self.object.user.id:
            return redirect('/accounts/profile')
        return super(ResumeEditTools, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ResumeEditTools, self).get_context_data(**kwargs)

        mytools = ResumeTool.objects.filter(resume__id=self.object.id).values('tool__slug')
        context['mytools'] = [ obj['tool__slug'] for obj in mytools ]
        
        tools = Tool.objects.all().values_list('slug', 'name', 'id')
        context['tools'] = tools.order_by('name')
        return context



class PreferenceView(ProtectedMixin, UpdateView):
    model = UserProfile
    form_class = PreferenceForm
    success_url = '/accounts/profile/'
    template_name = 'resume/preference_detail.html'


class ResumePublicListView(ListView):
    model = Resume
    template_name = 'resume/public_list.html'
    datas = ResumeOnline.objects.all()
    queryset = datas.values_list('resume__id',
                                 'resume__title',
                                 'resume__date_published')

    def get_queryset(self):
        try:
            toolstr = self.request.GET['t']
        except:
            toolstr = ''

        tools = clean_urlfilter(toolstr)
        resumes = self.queryset
        if len(tools):
            resumes = resumes.filter(tools__contains=tools)

        return resumes.order_by('-pk')

    def get_context_data(self, **kwargs):
        context = super(ResumePublicListView, self).get_context_data(**kwargs)
        # adding a values superseed the first
        resumes = self.get_queryset().values('resume_id')

        try:
            toolstr = self.request.GET['t']
            context["filters"] = toolstr.split(',')
            context["urlfilter"] = toolstr.replace(',,','')
        except:
            toolstr = ''

        context['tools'] = ResumeTool.objects.filter(resume__in=resumes).values_list('tool__slug', 'tool__name').annotate(numtool=Count('tool')).order_by('tool__name')

        return context


class ResumeEmploiPublicListView(ResumePublicListView):

    def get_queryset(self):
        queryset = super(ResumeEmploiPublicListView, self).get_queryset()
        resumes = queryset.filter(resume__family=settings.FAMILY_EMPLOI)
        return resumes.order_by('-pk')


class ResumeStagePublicListView(ResumePublicListView):

    def get_queryset(self):
        queryset = super(ResumeStagePublicListView, self).get_queryset()
        resumes = queryset.filter(resume__family=settings.FAMILY_STAGE)
        return resumes.order_by('-pk')


class ResumeMissionPublicListView(ResumePublicListView):

    def get_queryset(self):
        queryset = super(ResumeMissionPublicListView, self).get_queryset()
        resumes = queryset.filter(resume__family=settings.FAMILY_MISSION)
        return resumes.order_by('-pk')


class ResumeNew(ProtectedMixin, CreateView):
    model = Resume
    form_class = ResumeForm
    success_url = '/accounts/profile/'
    template_name = 'resume/resume_new.html'

    def get_initial(self):
        initial = super(ResumeNew, self).get_initial()
        initial.update({'email': self.request.user.email,
                        'firstname': self.request.user.first_name,
                        'name': self.request.user.last_name,
                        'user': self.request.user.id})
        return initial

    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.user_id = self.request.user.id
        self.object.save()

        return super(ResumeNew, self).form_valid(form)


class ExperienceNew(ProtectedMixin, CreateView):
    form_class = ExperienceForm
    template_name = 'resume/experience_new.html'

    def get_initial(self):
        initial = super(ExperienceNew, self).get_initial()
        initial.update({'resumid': self.kwargs['pk']})
        return initial

    def form_valid(self, form):
        res = Resume.objects.filter(user=self.request.user,
                                    pk=form.cleaned_data['resumid'])[0]

        self.object = form.save(commit=False)
        self.object.resume_id = res.id
        self.object.save()

        self.success_url = '/cv/{}/edit/'.format(res.id)
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.

        return super(ExperienceNew, self).form_valid(form)


class ExternalView(ProtectedMixin, ListView):
    model = UserRessource
    template_name = 'resume/external_list.html'

    def get_queryset(self):
        queryset = super(ExternalView, self).get_queryset()

        return queryset.select_related('ressource').order_by('ressource__name')


def external_delete(request, pk):
    """Delete a external ressource
    """
    UserRessource.objects.filter(pk=pk, user=request.user).delete()
    return redirect(reverse('resume_external'))


def addtool(request, pk, toolid):
    """Add a tool to a resume

    Security : check the resume owner id
    """
    resume = get_object_or_404(Resume, pk=pk)
    tool = get_object_or_404(Tool, pk=toolid)
    if request.user.id == resume.user.id:
        try:
            rest = ResumeTool.objects.create(resume=resume, tool=tool)
            result = rest.id
        except:
            result = 0
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404


def deltool(request, pk, toolid):
    """Delete a tool from a resume

    Security : check the resume owner id
    """
    resume = get_object_or_404(Resume, pk=pk)
    tool = get_object_or_404(Tool, pk=toolid)
    if request.user.id == resume.user.id:
        try:
            ResumeTool.objects.filter(resume=resume, tool=tool).delete()
            result = 0
        except:
            result = 1
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404


def publish(request, pk):
    """Publish the resume online
    """
    resume = get_object_or_404(Resume, pk=pk)
    if request.user.id == resume.user.id:
        resume.publish()
        return redirect(reverse('profile'))
    else:
        raise PermissionDenied


def unpublish(request, pk):
    """Unpublish a resume
    """
    resume = get_object_or_404(Resume, pk=pk)
    if request.user.id == resume.user.id:
        resume.unpublish()
        return redirect(reverse('profile'))
    else:
        raise PermissionDenied

def stats_views(request, pk):
    """Statistics view
    """
    datas = []
    rviews = ResumeStat.objects.values('date').filter(resume=pk).annotate(Count('date')).order_by('date')

    for dat in rviews:
        datas.append({"x": int((dat['date']).strftime('%s')), "y": dat['date__count']})

    return HttpResponse(json.dumps(datas), mimetype='application/json')
