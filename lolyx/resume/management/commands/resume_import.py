#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Import old database
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from lolyx.resume.models import Resume, ResumeTool
from lolyx.resume.models import ResumeLang
from lolyx.resume.models import ResumeRegion
from lolyx.llx.models import Tool
from lolyx.llx.models import Lang
from lolyx.llx.models import UserProfile
from django.db.models import Max
from django.db import connection
import MySQLdb


def decenc(data):
    if data:
        return data.decode('iso-8859-1').encode('utf8')
    else:
        return None


def notnullstr(data):
    if data is None:
        return ''
    else:
        return data


class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.db = self.mysqlconnect()
        Resume.objects.all().delete()
        ResumeTool.objects.all().delete()
        ResumeRegion.objects.all().delete()

        self.readdatas()
        # Update resume sequence
        self.update_sequences()

    def mysqlconnect(self):
        db = MySQLdb.connect(host="localhost", db="lolix")
        return db

    def mytool(self, id):
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT * FROM outil",
                          "WHERE fk_cand=%s"])

        cur.execute(query, (id, ))
        return cur.fetchall()

    def readdatas(self):
        # you must create a Cursor object. It will let
        #  you execute all the queries you need
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT l.login, l.clearpass, c.*",
                          "FROM candidat AS c",
                          "INNER JOIN login as l ON c.id = l.id",
                          "ORDER BY datec DESC",
                          "LIMIT %s"])

        limit = 30

        cur.execute(query, (limit, ))
        i = 0
        for row in cur.fetchall() :
            i = i + 1
            print i, row['idp']
            self.create_resume(row)

        print query

    def update_sequences(self):
        # you must create a Cursor object. It will let
        #  you execute all the queries you need

        idmax = Resume.objects.all().aggregate(Max('id')).values()
        print "id max %s" % (idmax[0])

        cursor = connection.cursor()
        qry = "ALTER SEQUENCE resume_resume_id_seq RESTART WITH %s";
        cursor.execute(qry, [idmax[0] + 1])
        cursor.close()


    def contrat(self, idc):
        if idc == 3:
            return settings.FAMILY_STAGE
        elif idc == 4:
            return settings.FAMILY_MISSION
        else:
            return settings.FAMILY_EMPLOI

    def poste_resume(self, id):
        cur = self.db.cursor()

        query = " ".join(["SELECT c.libelle FROM poste_resume p, c_poste c",
                          "WHERE fk_cand=%s",
                          "AND p.fk_poste = c.id",
                          "ORDER BY pref ASC"])

        cur.execute(query, (id, ))
        datas = cur.fetchall()
        cur.close()
        return datas

    def langs(self, id):
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT * FROM lang",
                          "WHERE fk_cand=%s"])

        cur.execute(query, (id, ))
        return cur.fetchall()


    def regions(self, id):
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT * FROM region_resume",
                          "WHERE fk_cand=%s"])

        cur.execute(query, (id, ))
        alls = cur.fetchall()
        cur.close()
        return alls

    def create_resume(self, data):
        oldid = data["idp"]
        login = decenc(data["login"])
        email = decenc(data["email"])
        user = None

        if email is None:
            email = "candidat-%d@lolix-legacy.org" % (oldid)

        try:
            user, create = User.objects.get_or_create(username=login, email=email)
        except:
            print "login=%s email=%s failed" % (login, email)

        if (user):
            userp = UserProfile.objects.get(user=user)
            userp.status = settings.USER_PROFILE_CANDIDAT
            userp.save()

            status = data["active"]
            title = decenc(data["poste"])
            name = notnullstr(decenc("nom"))
            firstname = notnullstr(decenc("prenom"))
            poste_desc = decenc(data["poste_desc"])
            fk_contrat = data["fk_contrat"]

            if title is None:
                datas = self.poste_resume(oldid)
                try:
                    title = decenc(datas[0][0])
                except:
                    pass

            newr = Resume(id=oldid,
                          title=title,
                          name=name,
                          firstname=firstname,
                          user_id=user.id,
                          status=status,
                          poste_desc_markup=0,
                          family=self.contrat(fk_contrat))

            if poste_desc is not None:
                newr.poste_desc = poste_desc

            newr.save()

            tools = []
            for tool in self.mytool(oldid):
                tools.append(ResumeTool(resume=newr,
                                        level=tool['fk_niveau'],
                                        tool=Tool.objects.get(pk=tool['fk_outil'])))
            ResumeTool.objects.bulk_create(tools)
            #
            # Import des langues
            #
            langs = []
            for lang in self.langs(oldid):
                langs.append(ResumeLang(resume=newr,
                                        level=lang['fk_niv'],
                                        lang=Lang.objects.get(pk=lang['fk_lang'])))
            ResumeLang.objects.bulk_create(langs)
