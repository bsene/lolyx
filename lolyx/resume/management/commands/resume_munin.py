#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output stats for munin
"""
from django.core.cache import cache
from django.core.management.base import BaseCommand
from lolyx.resume.models import Resume


class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.count(0)
        self.count(1)
        self.count(2)

    def count(self, family):

        labels = ['emploi', 'stage', 'mission']

        nb_resume = Resume.objects.filter(family=family).count()

        self.stdout.write("%s.value %s\n" % (labels[family], nb_resume))

        cache.set('resume_{}_nb_online'.format(labels[family]),
                                               nb_resume, 3600)
