#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Create sample resumes

A resume is published for a fixed number of days defined in settings,
after this period if the resume is still online it will be unpublished
"""
import logging
from datetime import datetime
from django.db import connection
from django.core.mail import send_mail
from django.conf import settings
from django.core.management.base import BaseCommand
from lolyx.llx.models import Tool
from lolyx.llx.models import Lang
from lolyx.resume.models import Resume
from lolyx.resume.models import ResumeLog
from lolyx.resume.models import ResumeExperience
from lolyx.resume.models import ResumeTool
from lolyx.resume.models import ResumeLang
from django.contrib.auth.models import User
from lolyx.llx.models import UserProfile
from faker import Faker
from optparse import make_option
from random import randrange

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Unpublished resume that have expired'

    option_list = BaseCommand.option_list + (
        make_option("-n",
                    "--nbvalues",
                    dest="nbvalues",
                    type="int",
                    help="number of values to input",
                    default=1),
        )

    def force_emailverified(self):
        sql = "UPDATE account_emailaddress SET verified = true"
        cursor = connection.cursor()
        cursor.execute(sql)

    def handle(self, *args, **options):
        """
        Handle the command
        """
        if not settings.DEBUG:
            print "Debug tools, quit now\n"
            exit(0)

        f = Faker()
        fid = self.create_resume(options['nbvalues'], f)
        self.create_resumeexperience(options['nbvalues'], fid, f)


    def next_id(self, sql):
        """Read the last used id from the sequence, return next usable id
        """
        cursor = connection.cursor()
        cursor.execute(sql)
        ids = cursor.fetchone()
        nextval = ids[0] + 1
        return nextval

    def create_resume(self, nb, f):
        i = 0
        sql = "SELECT last_value FROM auth_user_id_seq"
        firstid = self.next_id(sql)
        
        while i < nb:
            i = i + 1
            user, create = User.objects.get_or_create(username="login%s" % (i), email="email%s" % (i))

            userp = UserProfile.objects.get(user=user)
            userp.status = settings.USER_PROFILE_CANDIDAT
            userp.save()

            res = Resume.objects.create(user=user,
                                        family=randrange(3),
                                        title=" ".join(f.words(3)),
                                        poste_desc=" ".join(f.words(50)))

            for tool in Tool.objects.all().order_by('?')[:1+randrange(9)]:
                ResumeTool.objects.create(resume=res,
                                          tool=tool)

            for lang in Lang.objects.all().order_by('?')[:1+randrange(2)]:
                ResumeLang.objects.create(resume=res,
                                          lang=lang)                

            res.publish()

            print res.title, res.family

        return firstid

    def create_resumeexperience(self, nb, fid, f):
        i = 0
        batch = 100
        ibatch = 0
        resumes = []
        while i < nb:
            resid = randrange(nb) + fid
            resumes.append(ResumeExperience(resume_id=resid,
                                            date_deb=f.date(),
                                            date_fin=f.date(),
                                            title=" ".join(f.words()),
                                            desc=" ".join(f.words(35)),
                                            ))

            if ibatch > batch:
                ResumeExperience.objects.bulk_create(resumes)
                ibatch = 0
                             
            i = i + 1

        if len(resumes) > 0:
            ResumeExperience.objects.bulk_create(resumes)
