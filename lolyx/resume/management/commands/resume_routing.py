#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Route all resumes
"""
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from lolyx.llx.models import UserProfile
from lolyx.resume.models import Resume
from lolyx.job.models import Job
from lolyx.routing.tasks import route


class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        profiles = UserProfile.objects.all().exclude(lon__exact=0.0)
        jobs = Job.objects.all().exclude(lon__exact=0.0)

        for res in profiles:
            for job in jobs:
                print res.lon, res.lat, job.lon, job.lat
                tid = route.delay((res.lon, res.lat), (job.lon, job.lat))
