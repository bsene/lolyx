# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for ResumeExperience object
"""
from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from django.db import IntegrityError
from lolyx.resume.models import Resume, ResumeExperience


class ResumeExperienceTests(TestCase):  # pylint: disable-msg=R0904
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        ResumeExperience.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_create(self):
        """
        Create a simple resume with one experience
        """
        resume = Resume.objects.create(title='resume title',
                                       user=self.user)

        exp = ResumeExperience.objects.create(resume=resume,
                                              title ='foobar',
                                              date_deb=datetime.now(),
                                              date_fin=datetime.now(),
                                              desc='lorem',
                                              desc_markup=1)

        self.assertTrue(exp.id > 0)
