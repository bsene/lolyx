# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for lolyx.resume management commands
"""
from datetime import datetime, timedelta
from StringIO import StringIO
from django.conf import settings
from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth.models import User
from lolyx.resume.models import Resume


class CommandTests(TestCase):
    """
    Test the management commands in usermgt
    """
    def setUp(self):
        """
        Init
        """
        Resume.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_resume_expire(self):
        """The cron command that expired the resumes

        One off the resume resume must be unpublished the other one no
        """
        expdate = datetime.now() - timedelta(hours=1)
        exp = Resume.objects.create(user=self.user,
                                    title='Senior admin',
                                    status=settings.RESUME_STATUS_PUBLISHED,
                                    date_expired=expdate)

        expdate = datetime.now() + timedelta(hours=1)

        notexp = Resume.objects.create(user=self.user,
                                       title='Senior admin',
                                       status=settings.RESUME_STATUS_PUBLISHED,
                                       date_expired=expdate)

        content = StringIO()
        call_command('resume_expire', stdout=content)
        content.seek(0)

        jby = Resume.objects.get(pk=exp.id)
        jbn = Resume.objects.get(pk=notexp.id)

        self.assertEqual(jby.status, settings.RESUME_STATUS_EDITION)
        self.assertEqual(jbn.status, settings.RESUME_STATUS_PUBLISHED)

    def test_resume_import(self):
        """Import the old database

        TODO : write tests
        """
        content = StringIO()
        call_command('resume_import', stdout=content)
        content.seek(0)

    def test_munin(self):
        """
        The munin command

        Count number of benchs and run and print result on output
        with munin format
        """
        Resume.objects.create(title='Senior admin',
                              user=self.user,
                              family=1)

        attend = 'resume.value 0\nresume.value 1\nresume.value 0\n'

        content = StringIO()
        call_command('resume_munin', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)
