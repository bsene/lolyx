
$(document).ready(
    // Typehead initialisation
    function() {
	$.get('/ressources/typeahead/t/',
	      function(data){
		  $("#tool-typeahead").typeahead(
		      {
			  source: function(query, process) {

			      objects = [];
			      map = {
			      };
			      $.each(data, function(i, object) {
					 map[object.label] = object;
					 objects.push(object.label);
				     });
			      process(objects);
			  },
			  updater: function(item) {
			      var id = 0;
			      $.each(data, function(i, object) {
					 if (item == object.label) {
					     id = object.id;
					 }
				     });
			      if (id > 0) {
				  var urladd = 'addtool/' + id;
				  $.get(urladd,
					function (data) {
					    /* if the tool is correctly added
					     * result is > 0
					     */
					    if (data.result > 0) {					
						var spt = '<span class="restool">{{item}}</span>';
						var span = Mustache.render(spt,
									   {item: item});
						$('#resume-tools').append(span);
					    }
					    $('#tool-typeahead').val('');
					}, 'json');
			      }
			      return item;
			  }


		      }
		  );
	      },'json');
    }
);