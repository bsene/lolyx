==========
Cron tasks
==========

User
----

La commande suivante doit être exécutée à minima toutes les heures

``./manage.py user_points``


Données externes
----------------

La commande suivante doit être exécutée à minima toutes les heures

``./manage.py external_update``

Expiration des offres
---------------------

La commande suivante doit être exécutée à minima **une fois par jour**,
cette commande génère l'envoi d'emails.

``./manage.py job_expire``

Expiration des cv
-----------------

La commande suivante doit être exécutée à minima **une fois par jour**,
cette commande génère l'envoi d'emails.

``./manage.py resume_expire``

Statistiques
------------

Les commandes suivantes sont utilisées pour retourner des statistiques
pour munin mais maintiennent également à jour des données dans le
cache, elles doivent être éxécutées régulièrement si munin ne les
lancent pas directement.

 * ``./manage.py resume_munin``

 * ``./manage.py job_munin``

Outils
------

Les commandes suivantes sont utilisées pour mettre à jour la côte de
chaque outil afin de pouvoir proposer les plus fréquemement utilisés
dans les formulaires de saisies. La commande doit tourner une
**fois par jour**, la nuit idéalement.

 * ``./manage.py tool_odds``

Génération du templates ulule
-----------------------------

Génère la liste des sociétés qui ont participées à la campagne de
financement participatif. Les sociétés sont listées dans un ordre
aléatoire.

 * ``./manage.py companies_ulule``
