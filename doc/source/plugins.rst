=======
Plugins
=======

Un ensemble de plugin est définit dans l'application ``external``
afin de pouvoir enrichir les informations pour un utilisateur
particulier. Cet utilisateur peut aussi bien être une société qu'un
candidat.

Fonctionnement
==============

Un utilisateur peut associer à son compte un plugin avec un élément
d'identification. Régulièrement une tâche est exécutée et met à jour
les informations récupérées et les stocke dans la base de données.

Mise à jour
===========

Lors d'un appel d'API le résultat de la commande HTTP est stockée afin
de n'appeler que les urls valides. Le filtre dans la commande
external_update se fait sur un Return-Code==200.

Ajouter un plugin
=================

Si vous souhiatez ajouter un plugin que vous le developpiez vous même
ou que vous souhiatiez qu'il le soit par un contributeurs, ajouter une
entrée dans le `système de ticket <https://gitlab.com/rodo/lolyx/issues>`_ de gitlab. 
Vous pouvez vous inspirer du ticket pour le `plugin twitter <https://gitlab.com/rodo/lolyx/issues/3>`_.

Plugins existants
=================

Sans API
--------

* Advogato
* `April Trombinoscope <http://www.april.org/>`_
* `Gitlab <https://gitlab.com/>`_
* `Gitorious <https://gitorious.org/>`_
* Gna
* Lanyrd
* Launchpad
* Linuxfr
* ohloh
* `openclipart <https://openclipart.org/>`_
* Savannah
* Slideshare
* Sourceforge
* Transifex
* Wikipedia France


Avec API
--------

* `Bitbucket <https://bitbucket.org>`_
* `GitHub <https://github.com/>`_
* `OpenStreetMap <http://www.openstreetmap.org>`_
