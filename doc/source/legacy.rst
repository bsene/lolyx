

+-------------------+--------------+------+-----+---------+----------------+
| Field             | Type         | Null | Key | Default | Extra
|
+-------------------+--------------+------+-----+---------+----------------+
 0 | idp               | int(11)      | NO   | PRI | NULL    |auto_increment |
 1 | id                | varchar(32)  | YES  |     | NULL    ||
 2 | intern            | tinyint(1)   | YES  |     | 1       ||
 3 | cjn               | tinyint(1)   | YES  |     | 1       ||
 4 | active            | smallint(6)  | YES  |     | 0       ||
 5 | site              | smallint(6)  | NO   | MUL | 0       ||
 6 | datec             | datetime     | YES  |     | NULL    ||
 7 | datem             | datetime     | YES  |     | NULL    ||
 8 | datea             | datetime     | YES  |     | NULL    ||
 9 | dated             | datetime     | YES  |     | NULL    ||
10 | datel             | datetime     | YES  |     | NULL    ||
11 | type              | smallint(6)  | YES  |     | NULL    ||
12 | fk_civ            | int(11)      | YES  |     | 0       ||
13 | nom               | varchar(50)  | YES  |     | NULL    ||
14 | prenom            | varchar(30)  | YES  |     | NULL    ||
15 | naiss             | int(11)      | YES  |     | NULL    ||
16 | naissyear         | int(11)      | YES  |     | NULL    ||
17 | address           | varchar(255) | YES  |     | NULL    ||
18 | cp                | varchar(10)  | YES  |     | NULL    ||
19 | ville             | varchar(50)  | YES  |     | NULL    ||
20 | fk_pays           | int(11)      | YES  |     | 0       ||
21 | email             | varchar(255) | YES  |     | NULL    ||
22 | url               | varchar(255) | YES  |     | NULL    ||
23 | tel               | varchar(20)  | YES  |     | NULL    ||
24 | telpro            | varchar(20)  | YES  |     | NULL    ||
25 | fax               | varchar(20)  | YES  |     | NULL    ||
26 | mobile            | varchar(20)  | YES  |     | NULL    ||
27 | poste             | varchar(30)  | YES  |     | NULL    ||
28 | poste_desc        | text         | YES  |     | NULL    ||
29 | fk_contrat        | int(11)      | YES  |     | 0       ||
30 | cduree            | varchar(10)  | YES  |     | NULL    ||
| fk_duree          | int(11)      | YES  |     | 0       |
|
| dureetp           | varchar(10)  | YES  |     | NULL    |
|
| region            | varchar(30)  | YES  |     | NULL    |
|
| secteur           | varchar(40)  | YES  |     | NULL    |
|
| dispo             | varchar(25)  | YES  |     | NULL    |
|
| salaire           | varchar(40)  | YES  |     | NULL    |
|
| divers            | text         | YES  |     | NULL    |
|
| viewed            | int(11)      | YES  |     | 0       |
|
| viewed_last       | int(11)      | YES  |     | 0       |
|
| sent              | smallint(6)  | YES  |     | -1      |
|
| fk_anexpe         | smallint(6)  | YES  |     | 0       |
|
| fk_effectif       | smallint(6)  | YES  |     | 0       |
|
| anon              | tinyint(1)   | YES  |     | 0       |
|
| deacmeth          | char(1)      | YES  |     | NULL    |
|
| fk_modecontact    | smallint(6)  | YES  |     | 0       |
|
| moderech          | char(1)      | YES  |     | a       |
|
| pubkey            | varchar(32)  | YES  |     | NULL    |
|
| reminder          | tinyint(1)   | YES  |     | 1       |
|
| newsletter        | tinyint(1)   | YES  |     | 1       |
|
| sfuser            | varchar(32)  | YES  |     | NULL    |
|
| savannah_username | varchar(255) | YES  |     | NULL    |
|
| gna_username      | varchar(255) | YES  |     | NULL    |
|
| advogato_username | varchar(255) | YES  |     | NULL    |
|
| reponsenet        | varchar(32)  | YES  |     | NULL    |
|
| ohloh             | varchar(32)  | YES  |     | NULL    |
|
| stg_debut         | datetime     | YES  |     | NULL    |
|
| stg_fin           | datetime     | YES  |     | NULL    |
|
| fk_stg_type       | smallint(6)  | YES  |     | NULL    |
|
| rmll09            | smallint(6)  | YES  |     | 0       |
|
+-------------------+--------------+------+-----+---------+----------------+
