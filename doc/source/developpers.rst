=============
Developpement
=============

Initialiser une instance de Lolyx
---------------------------------

Le one liner suivant est utilisé pour créer une nouvelle base de
données et charger des données de développement. Ne pas utiliser cette
commande telle quelle si vous avez mis à jour des settings par défaut.

``rm /tmp/lolyx.sqlite  ; ./manage.py syncdb --noinput; ./manage.py loaddata lolyx/llx/fixtures/dev_data.json``

Une fois la base crée il faut également mettre à jour le cache avec
les commandes suivantes

 * ``./manage.py resume_munin``

 * ``./manage.py job_munin``


Exécuter les tests
------------------

Avant de crééer une pull request assuré vous que tous les tests
passent en exécutant la commande

``./manage.py test llx job resume external``
