.. Lolyx documentation master file, created by
   sphinx-quickstart on Mon Jan  6 14:24:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lolyx's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   moderation
   plugins
   crontasks
   celery
   developpers
   legacy

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

